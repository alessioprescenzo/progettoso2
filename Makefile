CC=gcc
CCOPTS= -Wall --std=gnu99 -O3
AR=ar


HEADERS=bitmap.h\
		disk_driver.h\
		simplefs.h

OBJS=bitmap.o\
	disk_driver.o\
	simplefs.o
	
LIBS=libFS.a

BINS= simplefs_test

.phony: clean all

all: $(LIBS) $(BINS)

%.o:	%.c $(HEADERS)
	$(CC) $(CCOPTS) -c -o $@  $<

libFS.a: $(OBJS) $(HEADERS)
	$(AR) -rcs $@ $^
	$(RM) $(OBJS)

simplefs_test:	simplefs_test.c $(LIBS)
	$(CC) $(CCOPTS) -o $@ $^
clean:
	rm -rf *.o *~ $(LIBS) $(BINS) ./FAPDisk ./benchmark

run:
	./$(BINS)


benchmark:	benchmark_main.c $(LIBS)
	$(CC) $(CCOPTS) -o $@ $^ && ./benchmark