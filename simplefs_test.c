#include "simplefs.h"
#include "disk_driver.h"
#include "bitmap.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>
#include <unistd.h>
#include <string.h>

#define MAX_FILE_OPEN 64

int main(int agc, char** argv) {

char* commands[] = {"formatdisk", "createfile", "ls", "openfile", "closefile", "writefile", "readfile", "seek", "cd", "mkdir", "remove", "tree", "printdir", "printdisk", "printfile", "quit"};
char* howtouse[] = {
  "no arguments needed", "followed by the name of file", "no arguments needed", "followed by the name of file", "followed by the name of file",
  "followed by the name of file, char to write, n of repetion", "followed by the name of file, size of data", "followed by the name of file, position of pointer",
  "followed by the name of directory", "followed by the name of directory", "followed by the name of file/directory", "no arguments needed", "no arguments needed",
  "no arguments needed", "followed by the name of file", "no arguments needed"  
};

char welcome[] = {
 "*************************************************************************************************************************\n"
 "*                                                                                                                       *\n"
 "*                                                                                                                       *\n"
 "*          @@@  @@@  @@@  @@@@@@@@  @@@        @@@@@@@   @@@@@@   @@@@@@@@@@   @@@@@@@@      @@@@@@   @@@  @@@          *\n"
 "*          @@@  @@@  @@@  @@@@@@@@  @@@       @@@@@@@@  @@@@@@@@  @@@@@@@@@@@  @@@@@@@@     @@@@@@@@  @@@@ @@@          *\n"
 "*          @@!  @@!  @@!  @@!       @@!       !@@       @@!  @@@  @@! @@! @@!  @@!          @@!  @@@  @@!@!@@@          *\n"
 "*          !@!  !@!  !@!  !@!       !@!       !@!       !@!  @!@  !@! !@! !@!  !@!          !@!  @!@  !@!!@!@!          *\n"
 "*          @!!  !!@  @!@  @!!!:!    @!!       !@!       @!@  !@!  @!! !!@ @!@  @!!!:!       @!@  !@!  @!@ !!@!          *\n"
 "*          !@!  !!!  !@!  !!!!!:    !!!       !!!       !@!  !!!  !@!   ! !@!  !!!!!:       !@!  !!!  !@!  !!!          *\n"
 "*          !!:  !!:  !!:  !!:       !!:       :!!       !!:  !!!  !!:     !!:  !!:          !!:  !!!  !!:  !!!          *\n"
 "*          :!:  :!:  :!:  :!:       :!:       :!:       :!:  !:!  :!:     :!:  :!:          :!:  !:!  :!:  !:!          *\n"
 "*           :::: :: :::   :: ::::   :: ::::   ::: :::   ::::: ::  :::     ::   :: ::::      ::::: ::  ::   ::           *\n"
 "*             :: :  : :    : :: ::  : :: : :   :: :: :   : :  :    :      :    : :: ::       : :  :   ::   :            *\n"
 "*                                                                                                                       *\n"
 "*                                                                                                                       *\n"
 "*             @@@@@@@@   @@@@@@   @@@@@@@      @@@@@@    @@@@@@      @@@@@@@  @@@@@@@@   @@@@@@   @@@@@@@               *\n"
 "*             @@@@@@@@  @@@@@@@@  @@@@@@@@     @@@@@@@  @@@@@@@@     @@@@@@@  @@@@@@@@  @@@@@@@   @@@@@@@               *\n"
 "*             @@!       @@!  @@@  @@!  @@@         @@@       @@@       @@!    @@!       !@@         @@!                 *\n"
 "*             !@!       !@!  @!@  !@!  @!@         @!@      @!@        !@!    !@!       !@!         !@!                 *\n"
 "*             @!!!:!    @!@!@!@!  @!@@!@!      @!@!!@      !!@         @!!    @!!!:!    !!@@!!      @!!                 *\n"
 "*             !!!!!:    !!!@!!!!  !!@!!!       !!@!@!     !!:          !!!    !!!!!:     !!@!!!     !!!                 *\n"
 "*             !!:       !!:  !!!  !!:              !!:   !:!           !!:    !!:           !:!     !!:                 *\n"
 "*             :!:       :!:  !:!  :!:              :!:  :!:            :!:    :!:           !:!     :!:                 *\n"
 "*              ::       ::   :::   ::          :: ::::  :: :::::        ::     :: ::::  :::: ::      ::                 *\n"
 "*               :        :   : :   :            : : :   :: : :::         :     : :: ::   :: : :       :                 *\n"
 "*                                                                                                                       *\n"
 "*                                                                                                                       *\n"
 "*************************************************************************************************************************\n"
 };

  char temp;

  DiskDriver disk = {0};
  SimpleFS fs = {0};

  printf("\n%s\n", welcome);
  printf("Credits: Alessio Prescenzo & Marco Mormando\n\n");
  printf("\nThis is an interactive test of our FileSystem, follow the instructions and enjoy it!\n\n");
  
  int num_blocks = 0;

  if(access("FAPDisk", F_OK) == -1) {
    // file doesn't exists
    printf("Enter number of disk's blocks (remember BLOCK_SIZE: %dB): ", BLOCK_SIZE);
    scanf("%d", &num_blocks);
    printf("\nYou chose: %d\n\n", num_blocks);
    scanf("%c",&temp); // temp statement to clear buffer
  } 
  
  printf("Opening disk 'discoprova'... Formatting if it doesn't exist!\n\n");
  
  DiskDriver_init(&disk,"FAPDisk",num_blocks);
  DirectoryHandle* curr_dir = SimpleFS_init(&fs, &disk);

  printf("Now the disk is ready!\n\n");
  printDisk(&disk);

  printf("\n\nYou are in dir %s\n", curr_dir->dcb->fcb.name);
  printDir(curr_dir);

  printf("\n\nThis is the list of operations allowed on our FileSystem, follow the instructions to use them:\n\n");
  for(int i=0; i<16; i++){
    printf("%d. %s: %s\n", i+1, commands[i], howtouse[i]);
  }
  printf("\n");

  int res;
  //for input string
  char* input = (char*)malloc(1024);
  //tokens of string (max n arg = 4)
  char** words = (char**)malloc(sizeof(char*)*4);
  //to store opened/created file
  int num_file_opened = 0;
  Entry_table_file_opened* array_file_opened[MAX_FILE_OPEN] = {0};

  while(1){
    printf("> ");
    memset(input, 0, 1024);
	  res = scanf("%[^\n]",input);
    scanf("%c",&temp); // temp statement to clear buffer
    if(res < 0){
      printf("An error occured in scanf... Closing all!\n");
      return -1;
    }
    int length_string = strlen(input);
    if(length_string == 0) {
      printf("\n");
      continue;
    }

    int num_words = 0;
    int idx_found = -1;
    char* pch = strtok (input," ");
    int i = 0;
    while(pch != NULL && i < 4){
      num_words++;
      words[i++] = pch;
      pch = strtok(NULL, " ");
    }

    if(strcmp("quit", words[0])==0) {
      if(num_file_opened > 0){
        printf("Closing all files before you go...\n");
        for(int i=0; i<num_file_opened; i++){
          free(array_file_opened[i]->filename);
          SimpleFS_close(array_file_opened[i]->fh);
          free(array_file_opened[i]);
          array_file_opened[i] = NULL;
        }
        num_file_opened = 0;
      }
      printf("It was a pleasure, bye bye :)\n\n");
      break;
    }
    else if(strcmp("tree", words[0])==0){
      SimpleFS_tree(&fs);
      printf("\n");
    }
    else if(strcmp("mkdir", words[0])==0){
      if(num_words < 2){
        printf("Missing name of directory to create\n\n");
        continue;
      }
      res = SimpleFS_mkDir(curr_dir, words[1]);
      if(res < 0) printf("Error in mkdir %s\n\n", words[1]);
      else printf("Directory %s created successfully\n\n", words[1]);
    }
    else if(strcmp("remove", words[0])==0){
      if(num_words < 2){
        printf("Missing name of directory or file to remove\n\n");
        continue;
      }
      res = SimpleFS_remove(curr_dir, words[1]);
      if(res < 0) printf("Error in remove %s\n\n", words[1]);
      else printf("Directory/file %s removed successfully\n\n", words[1]);
    }
    else if(strcmp("printdir", words[0])==0){
      printDir(curr_dir);
      printf("\n");
    }
    else if(strcmp("printdisk", words[0])==0){
      printDisk(&disk);
      printf("\n");
    }
    else if(strcmp("printfile", words[0])==0){
      if(num_words < 2){
        printf("Missing name of file\n\n");
        continue;
      }
      idx_found = -1;
      for(int i=0; i<num_file_opened; i++){
        if(strcmp(array_file_opened[i]->filename, words[1])==0 && 
            curr_dir->dcb->fcb.block_in_disk == array_file_opened[i]->fh->p_directory->fcb.block_in_disk){
              idx_found = i;
              break;
        }  
      }
      if(idx_found < 0) printf("File wasn't opened!\n\n");
      else{
        printFile(array_file_opened[idx_found]->fh);
        printf("\n");
      }
      
    }
    else if(strcmp("cd", words[0])==0){
      if(num_words < 2){
        printf("Missing name of directory\n\n");
        continue;
      }
      res = SimpleFS_changeDir(curr_dir, words[1]);
      if(res < 0) printf("Error in changedir %s\n\n", words[1]);
      else printf("Entered in %s\n\n", words[1]);
    }
    else if(strcmp("ls", words[0])==0){
      if(curr_dir->dcb->num_entries == 0){
        printf("No file or dir present in %s\n\n", curr_dir->dcb->fcb.name);
        continue;
      } 
      char** names = (char**)malloc(sizeof(char*)*curr_dir->dcb->num_entries);
      for(int i=0; i<curr_dir->dcb->num_entries; i++) names[i] = (char*)malloc(128);
      res = SimpleFS_readDir(names, curr_dir);
      if(res < 0) printf("Error in changedir %s\n\n", words[1]);
      else {
        printf("File or dir in %s:\n", curr_dir->dcb->fcb.name);
        for(int i=0; i<curr_dir->dcb->num_entries; i++){
          printf("- %s\n", names[i]);
        }
        printf("\n");
      }
      for(int i=0; i<curr_dir->dcb->num_entries; i++) free(names[i]);
      free(names);
    }
    else if(strcmp("formatdisk", words[0])==0){
      if(num_file_opened > 0){
        for(int i=0; i<num_file_opened; i++){
          free(array_file_opened[i]->filename);
          SimpleFS_close(array_file_opened[i]->fh);
          free(array_file_opened[i]);
          array_file_opened[i] = NULL;
        }
        num_file_opened = 0;
      }
      while(strcmp(curr_dir->dcb->fcb.name, "/") != 0) SimpleFS_changeDir(curr_dir, "..");
      SimpleFS_format(&fs);
      SimpleFS_syncCurrDir(curr_dir);
      printf("Disk formatted successfully... Did you have something to hide? :)\n\n");
    }
    else if(strcmp("createfile", words[0])==0){
      if(num_words < 2){
        printf("Missing name of file to create\n\n");
        continue;
      }
      //da conservare in una lista
      FileHandle* tmp = SimpleFS_createFile(curr_dir, words[1]);
      if(tmp == NULL) printf("Error in createfile %s\n\n", words[1]);
      else printf("File created successfully %s\n\n", words[1]);
      //ora uso la close
      SimpleFS_close(tmp);
    }
    else if(strcmp("openfile", words[0])==0){
      if(num_words < 2){
        printf("Missing name of file to open\n\n");
        continue;
      }
      idx_found = -1;
      for(int i=0; i<num_file_opened; i++){
        if(strcmp(array_file_opened[i]->filename, words[1])==0 && 
            curr_dir->dcb->fcb.block_in_disk == array_file_opened[i]->fh->p_directory->fcb.block_in_disk){
              idx_found = i;
              break;
        }  
      }
      if(idx_found >= 0){
        printf("File already opened!\n\n");
        continue;
      }
      FileHandle* tmp = SimpleFS_openFile(curr_dir, words[1]);
      if(tmp == NULL) printf("Error in openfile %s\n\n", words[1]);
      else {
        if(num_file_opened == MAX_FILE_OPEN){
          printf("Too files opened... Close something before and try againg\n\n");
          SimpleFS_close(tmp);
        }
        else{
          Entry_table_file_opened* entry = (Entry_table_file_opened*)malloc(sizeof(Entry_table_file_opened));
          entry->filename = (char*)malloc(128);
          memcpy(entry->filename, words[1], strlen(words[1])+1);
          entry->fh = tmp;
          array_file_opened[num_file_opened] = entry;
          num_file_opened++;
          printf("File opened successfully %s\n\n", words[1]);
        }
      }
    }
    else if(strcmp("closefile", words[0])==0){
      if(num_words < 2){
        printf("Missing name of file to close\n\n");
        continue;
      }
      if(num_file_opened == 0) printf("No files opened!\n\n");
      else{
        idx_found = -1;
        for(int i=0; i<num_file_opened; i++){
          if(strcmp(array_file_opened[i]->filename, words[1])==0 && 
             curr_dir->dcb->fcb.block_in_disk == array_file_opened[i]->fh->p_directory->fcb.block_in_disk){
               idx_found = i;
               break;
          }  
        }
        if(idx_found < 0) printf("File wasn't opened!\n\n");
        else{
          free(array_file_opened[idx_found]->filename);
          SimpleFS_close(array_file_opened[idx_found]->fh);
          free(array_file_opened[idx_found]);
          if(idx_found == num_file_opened-1){
            array_file_opened[num_file_opened-1] = NULL;
          }
          else{
            array_file_opened[idx_found] = array_file_opened[num_file_opened-1];
            array_file_opened[num_file_opened-1] = NULL;
          }
          num_file_opened--;
          printf("File %s closed successfully\n\n", words[1]);
        }
      }
    }
    else if(strcmp("writefile", words[0])==0){
      if(num_words < 2){
        printf("Missing name of file to write\n\n");
        continue;
      }
      if(num_words < 3){
        printf("Missing char to write\n\n");
        continue;
      }
      if(num_words < 4){
        printf("Missing size in bytes\n\n");
        continue;
      }
      if(num_file_opened == 0) printf("No files opened!\n\n");
      else{
        idx_found = -1;
        for(int i=0; i<num_file_opened; i++){
          if(strcmp(array_file_opened[i]->filename, words[1])==0 && 
             curr_dir->dcb->fcb.block_in_disk == array_file_opened[i]->fh->p_directory->fcb.block_in_disk){
               idx_found = i;
               break;
          }  
        }
        if(idx_found < 0) printf("File wasn't opened!\n\n");
        else{
          char to_write = words[2][0];
          int n = atoi(words[3]);
          char* buf = (char*)malloc(n);
          memset(buf, to_write, n);
          res = SimpleFS_write(array_file_opened[idx_found]->fh, buf, n);
          if(res < n) printf("Error in writing file %s (probably not enough space on disk)\n\n", words[1]);
          else printf("Wrote %d bytes successfully\n\n", n);
          SimpleFS_syncCurrDir(curr_dir);
          free(buf);
        }
      }
    }
    else if(strcmp("readfile", words[0])==0){
      if(num_words < 2){
        printf("Missing name of file to read\n\n");
        continue;
      }
      if(num_words < 3){
        printf("Missing size in bytes to read\n\n");
        continue;
      }
      if(num_file_opened == 0) printf("No files opened!\n\n");
      else{
        idx_found = -1;
        for(int i=0; i<num_file_opened; i++){
          if(strcmp(array_file_opened[i]->filename, words[1])==0 && 
             curr_dir->dcb->fcb.block_in_disk == array_file_opened[i]->fh->p_directory->fcb.block_in_disk){
               idx_found = i;
               break;
          }  
        }
        if(idx_found < 0) printf("File wasn't opened!\n\n");
        else{
          int size = atoi(words[2]);
          char* buf = (char*)malloc(size+1);
          memset(buf, 0, size);
          res = SimpleFS_read(array_file_opened[idx_found]->fh, buf, size);
          if(res < size) printf("Error in reading file %s (probably you want to read more than the size of file)\n\n", words[1]);
          else {
            printf("Read %d bytes successfully, see the result:\n", size);
            printf("%s\n\n", buf);
          }
          free(buf);
        }
      }
    }
    else if(strcmp("seek", words[0])==0){
      if(num_words < 2){
        printf("Missing name of file to seek\n\n");
        continue;
      }
      if(num_words < 3){
        printf("Missing value of pointer to set\n\n");
        continue;
      }
      if(num_file_opened == 0) printf("No files opened!\n\n");
      else{
        idx_found = -1;
        for(int i=0; i<num_file_opened; i++){
          if(strcmp(array_file_opened[i]->filename, words[1])==0 && 
             curr_dir->dcb->fcb.block_in_disk == array_file_opened[i]->fh->p_directory->fcb.block_in_disk){
               idx_found = i;
               break;
          }  
        }
        if(idx_found < 0) printf("File wasn't opened!\n\n");
        else{
          int pos = atoi(words[2]);
          res = SimpleFS_seek(array_file_opened[idx_found]->fh, pos);
          if(res < 0) printf("Error in seeking file %s\n\n", words[1]);
          else printf("Pointer set successfully\n\n");
        }
      }
      
    }
    else{
      printf("Command not supported yet, contact us for updates\n\n");
    }
  }

  free(words);
  free(input);
  free(curr_dir->p_directory);
  free(curr_dir->dcb);
  free(curr_dir);
  DiskDriver_flush(&disk);
  close(disk.fd);
  return 0;
}

