#include "simplefs.h"
#include "disk_driver.h"
#include "bitmap.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>
#include <unistd.h>
#include <string.h>
#include <time.h>



char* itoa(int value, char* result, int base) {
    // check that the base if valid
    if (base < 2 || base > 36) { *result = '\0'; return result; }

    char* ptr = result, *ptr1 = result, tmp_char;
    int tmp_value;

    do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
    } while ( value );

    // Apply negative sign
    if (tmp_value < 0) *ptr++ = '-';
    *ptr-- = '\0';
    while(ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
    }
    return result;
}
	


int main(int agc, char** argv){
    int res;
    DiskDriver disk = {0};
    SimpleFS fs = {0};

    printf("Initializing a 2 Gigabyte disk\n");

    DiskDriver_init(&disk,"FAPDisk",(int)4194304);
    DirectoryHandle* curr_dir = SimpleFS_init(&fs, &disk);
    printf("Creating 2 files sized about 1 gb each:\n\n");
    int size = (disk.header->free_blocks/2)*512  - 2;
    char* buf=(char*)malloc(size);
    memset(buf,64,size);
    
    clock_t start, end;
    double cpu_time_used;
    FileHandle* file;
    start = clock();
    file = SimpleFS_createFile(curr_dir,"1gb-test");
    res = SimpleFS_write(file,buf,size);
    if(res < 0) printf("Error in writing 1 gb file");
    SimpleFS_close(file);
    
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Wrote first 1 GB in %f seconds\n", cpu_time_used);
     
    start = clock();
    file = SimpleFS_createFile(curr_dir,"1gb-test-1");
    res = SimpleFS_write(file,buf,size);
    if(res < 0) printf("Error in writing 1 gb file");
    SimpleFS_close(file);
    
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Wrote second 1 GB in %f seconds\n", cpu_time_used);
    
    printf("Removing 2 gb worth of files...\n\n");

    start = clock();
    res = SimpleFS_remove(curr_dir,"1gb-test");
    if(res < 0) printf("Error in writing 1 gb file");

    res = SimpleFS_remove(curr_dir,"1gb-test-1");
    if(res < 0) printf("Error in writing 1 gb file");

    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Removed 2 GB in %f seconds\n\n", cpu_time_used);
    

    int i;
    char* buftmp = (char*)malloc(512);
    int blocks = ((disk.header->free_blocks)/1000)-3;

    printf("Creating writing and removing %d files:\n", blocks);
    start = clock();
    for(i = 0;i<blocks;i++){
        buftmp = itoa(i,buftmp,10);
        file = SimpleFS_createFile(curr_dir,buftmp);
        res = SimpleFS_write(file,buf,500000);
        SimpleFS_close(file);
    }
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Created and written %d files in %f seconds\n", i, cpu_time_used);

    start = clock();
    for(i = 0;i<blocks;i++){
        buftmp = itoa(i,buftmp,10);
        res = SimpleFS_remove(curr_dir,buftmp);
    }
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Removed %d files in %f seconds\n", i, cpu_time_used);
    
    free(curr_dir->dcb);
    free(curr_dir->p_directory);
    free(curr_dir);
    free(buftmp);
    free(buf);
    return 0;
}

