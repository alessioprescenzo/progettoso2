#pragma once
#include <stdint.h>
#include <stdio.h>

typedef struct{
  int num_bits;
  char* entries;
}  BitMap;

typedef struct {
  int entry_num; //index in array
  char bit_num; // offset of bit inside the array 
} BitMapEntryKey;

// converts a block index to an index in the array,
// and a char that indicates the offset of the bit inside the array
BitMapEntryKey BitMap_blockToIndex(int num);

// converts a bit to a linear index
int BitMap_indexToBlock(int entry, uint8_t bit_num);

// returns the index of the first bit having status "status"
// in the bitmap bmap, and starts looking from position start
int BitMap_get(BitMap* bmap, int start, int status);

// sets the bit at index pos in bmap to status
int BitMap_set(BitMap* bmap, int pos, int status);

//return 0 if bit is 0, otherwise 1
unsigned char BitMap_isBusy(BitMapEntryKey* entry, char* bitmap);

//debug a function
void debugBmap(BitMap* bmap);

//testing all functions
void BeEvilWhenTestingBitMap();