#include "bitmap.h"

/*NB
Quando passo un indice di blocco a BitMap_blockToIndex con num/8 ottengo indice in array bitmap con num%8
ottengo offset su char in array[num/8] di bitmap.
*/

// converts a block index to an index in the array,
// and a char that indicates the offset of the bit inside the array
BitMapEntryKey BitMap_blockToIndex(int num){
    int entry_num = num / 8; //index in array
    char bit_num = num % 8; // offset of bit inside the array 

    BitMapEntryKey to_ret;
    to_ret.entry_num = entry_num;
    to_ret.bit_num = bit_num; 

    return to_ret;
}

// converts a bit to a linear index
int BitMap_indexToBlock(int entry, uint8_t bit_num){
    return entry*8 + bit_num;
}

// returns the index of the first bit having status "status"
// in the bitmap bmap, and starts looking from position start
int BitMap_get(BitMap* bmap, int start, int status){
    if(status != 0 && status != 1) return -1;
    unsigned int idx = start/8;

    while(idx <= (bmap->num_bits)/8){
        unsigned char block = bmap->entries[idx];
        for(int i = 7; i >= 0; i--){
            int idx_bit = 8*idx + (7-i);
            if(idx_bit >= start && idx_bit < bmap->num_bits){
                char bit = (block >> i) % 2;
                if(bit == status){
                    return (7-i)+idx*8; //è perfetto
                }
            }
        } 
        idx++;
    }
    return -1;
}

// sets the bit at index pos in bmap to status
int BitMap_set(BitMap* bmap, int pos, int status){
    if(pos >= bmap->num_bits || pos < 0) return -1;
    if(status != 0 && status != 1) return -1;
    int idx = pos/8;
    char bit = pos % 8;
    char prev = bmap->entries[idx]; 
    char mask;
    if (status){
        mask = status << (7 - bit);
        bmap->entries[idx] = prev | mask;
    } 
    if (!status){
        mask = 0xFF - (1 << (7 - bit)); 
        bmap->entries[idx] = prev & mask;
    } 
    return 0; // se riuscito 0
}

unsigned char BitMap_isBusy(BitMapEntryKey* entry, char* bitmap){
    return ((unsigned char)bitmap[entry->entry_num] >> (7 - entry->bit_num)) % 2;
}

void debugBmap(BitMap* bmap){
    int idx = 0;
    while(idx <= (bmap->num_bits/8)){
        unsigned char block = bmap->entries[idx];
        for(int i = 7; i >= 0; i--){
            int idx_bit = 8*idx + (7-i);
            if(idx_bit < bmap->num_bits){
                char bit = (block >> i) % 2;
                printf("Element num: %d = %u\n", (7-i)+idx*8, bit);
            }
        }
        idx++;
    }

}

void BeEvilWhenTestingBitMap(BitMap* bmap){
  
  printf("Testing set...\n\n");
  printf("BitMap_set(bmap,5,1):\n");
  int res = BitMap_set(bmap,5,1);
  if(res == -1){
    printf("Error in set1\n\n");
  }else{
      debugBmap(bmap);
  }

  printf("\n\n");

  printf("BitMap_set(bmap,5,0):\n");
  res = BitMap_set(bmap,5,0);
  if(res == -1){
    printf("Error in set2\n\n");
  }else{
      debugBmap(bmap);
  }

  printf("\n\n");

  printf("BitMap_set(bmap,5,48):\n");
  res = BitMap_set(bmap,5,48);
  if(res == -1){
    printf("Error in set3\n\n");
  }else{
      debugBmap(bmap);
  }

  printf("\n\n");
  
  printf("BitMap_set(bmap,15,1):\n");
  res = BitMap_set(bmap,15,1);
  if(res == -1){
    printf("Error in set4\n\n");
  }else{
      debugBmap(bmap);
  }

  printf("\n\n");
  printf("Testing get on BitMap...\n");

  printf("Index: %d \n",BitMap_get(bmap,0,1));
  printf("Index: %d \n",BitMap_get(bmap,8,0));
  printf("Index: %d \n",BitMap_get(bmap,14,0));
  printf("Index: %d \n",BitMap_get(bmap,18,1));
  

  debugBmap(bmap);

}
