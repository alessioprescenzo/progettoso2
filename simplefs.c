#include "simplefs.h"

DirectoryHandle* SimpleFS_init(SimpleFS* fs, DiskDriver* disk){
    if(!disk || !fs) return NULL;
    int res;
    fs->disk=disk;
    if(fs->disk->header->first_free_block == fs->disk->header->bitmap_blocks){
        SimpleFS_format(fs);
        printf("Disk formatted\n\n");
    }
    else printf("Disk already formatted!\n\n");
    
    //create necessaries struct
    DirectoryHandle* d_h = (DirectoryHandle*)malloc(sizeof(DirectoryHandle));
    d_h->sfs = fs;
    d_h->dcb = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock)); 
    d_h->p_directory = NULL; 
    //create a buf for reading the entire block
    char tmp[BLOCK_SIZE] = {0};
    res = DiskDriver_readBlock(disk,tmp,disk->header->bitmap_blocks);
    if(res < 0){
        printf("Cannot read '/' root directory\n\n");
        exit(-1);
    }
    //copy from buf to struct
    memcpy(d_h->dcb,tmp,sizeof(FirstDirectoryBlock));
    d_h->current_block = &(d_h->dcb->header);
    d_h->pos_in_dir = 0;
    d_h->pos_in_block = disk->header->bitmap_blocks;
    
    //set curr and root idx
    fs->root=d_h->pos_in_block;
    fs->curr_dir=fs->root;

    return d_h;
}

void SimpleFS_format(SimpleFS* fs){
    int i,res;
    //resetting bitmap
    for(i=fs->disk->header->bitmap_blocks;i < fs->disk->header->num_blocks;i++){
        res = DiskDriver_freeBlock(fs->disk,i);
        if(res < 0){
            printf("Cannot free a block when formatting disk\n\n");
            exit(-1);
        }
    }

    char tmp[BLOCK_SIZE] = {0};
    FirstDirectoryBlock tmp_fdb = {0};
    tmp_fdb.header.previous_block = -1; //no prev block cause it's the first one
    tmp_fdb.header.next_block = -1; //no next block cause no data inside (Attenzione va messo a -1 dopo test)
    tmp_fdb.header.block_in_file = 0; //cause it's FCB
    tmp_fdb.fcb.directory_block = -1; //cause it's root
    tmp_fdb.fcb.block_in_disk = fs->disk->header->bitmap_blocks;
    memcpy(&(tmp_fdb.fcb.name),"/",2); //assign / to name
    tmp_fdb.fcb.size_in_bytes = 0; //cause it's content is null
    tmp_fdb.fcb.size_in_blocks = 1; //cause the struct is in 1 block
    tmp_fdb.fcb.is_dir = 1; //cause it's a dir
    tmp_fdb.num_entries = 0; // no files or dir inside (Attenzione da mettere a 0 dopo test)
    int size = ((BLOCK_SIZE - sizeof(BlockHeader) - sizeof(FileControlBlock) - sizeof(int))/sizeof(int));
    memset(&(tmp_fdb.file_blocks),0,size); //set to 0 the file_blocks entry
    memcpy(tmp,&tmp_fdb,sizeof(FirstDirectoryBlock));
    res = DiskDriver_writeBlock(fs->disk,tmp,tmp_fdb.fcb.block_in_disk);
    if(res < 0){
        printf("Cannot create the '/' root directory\n\n");
        exit(-1);
    }    
}

FileHandle* SimpleFS_createFile(DirectoryHandle* d, const char* filename){
    int i,res;

    if(d->sfs->disk->header->first_free_block < 0){
        printf("No free blocks available\n\n");
        return NULL;
    };

    char** names = malloc(sizeof(char*)*d->dcb->num_entries);
    for(i=0; i < d->dcb->num_entries; i++) names[i] = malloc(128);

    SimpleFS_readDir(names, d);

    //if there is already a file with filename exit
    for(int i=0; i < d->dcb->num_entries; i++) {
        if(strcmp(names[i], filename) == 0){
            printf("File already exist -> change name\n\n");
            for(int i=0; i<d->dcb->num_entries; i++) free(names[i]);
            free(names);
            return NULL;
        }
    }

    for(int i=0; i<d->dcb->num_entries; i++) free(names[i]);
    free(names);

    int idx_block_tmp = DiskDriver_getFreeBlock(d->sfs->disk,d->sfs->disk->header->first_free_block);
    int idx_max_ffb = (BLOCK_SIZE
	    -sizeof(BlockHeader)
		-sizeof(FileControlBlock)
		-sizeof(int))/sizeof(int);  
   
    
    char buffer_tmp[BLOCK_SIZE] = {0};

    if(d->dcb->num_entries+1 <= idx_max_ffb){
        //entra nel primo blocco
        d->dcb->file_blocks[d->dcb->num_entries] = idx_block_tmp;  //ok
    }
    else{
        int idx_max_fb = (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int); //max block for dir block (not first)
        int idx = (d->dcb->num_entries - idx_max_ffb);
        idx = idx % idx_max_fb; //if 0 create a dir block oterwhise add in last dir block      

        char read_block_dir_tmp[BLOCK_SIZE] = {0};
        DirectoryBlock* db_tmp = NULL;
        for(i = 1; i < d->dcb->fcb.size_in_blocks; i++){
            int idx_block_last_dirb = d->current_block->next_block;
            res = DiskDriver_readBlock(d->sfs->disk, read_block_dir_tmp, idx_block_last_dirb);
            if(res < 0){
                printf("Error when finding last block of directory\n\n");
                exit(-1);
            }
            db_tmp = (DirectoryBlock*)read_block_dir_tmp; //last directory block
            d->pos_in_block = idx_block_last_dirb; //id block n disk last db
            d->current_block = &(db_tmp->header);//block header of last db
        }

        if(idx == 0){//dovrò creare un dir block e collegarlo (uso idx_tmp e poi richiedo getfreeblock per file)
            DirectoryBlock to_add_last = {0};
            to_add_last.header.next_block = -1; //its last
            to_add_last.header.block_in_file = d->current_block->block_in_file + 1; //aggiungo un blocco
            to_add_last.header.previous_block = d->pos_in_block; //il pred è il blocco ex last
            memset(&(to_add_last.file_blocks), 0, idx_max_fb); //non serve manco
            
            d->current_block->next_block = idx_block_tmp; //lo rubo al file
            
            //devo salvare modifiche dei dir block creato
            memcpy(buffer_tmp, &to_add_last, sizeof(DirectoryBlock));
            res = DiskDriver_writeBlock(d->sfs->disk, buffer_tmp, idx_block_tmp);
            if(res < 0){
                printf("Error when writing the new block of directory\n\n");
                exit(-1);
            }

            if(d->sfs->disk->header->first_free_block < 0){ //non posso aggiungerci il file
                res = DiskDriver_freeBlock(d->sfs->disk, idx_block_tmp);
                if(res < 0){
                    printf("Error when freeing the new block of directory created before\n\n");
                    exit(-1);
                }
                printf("No free blocks available for file -> deleting update folder\n\n");
                exit(-1);
            };
            idx_block_tmp = DiskDriver_getFreeBlock(d->sfs->disk, d->sfs->disk->header->first_free_block);

            d->dcb->fcb.size_in_blocks += 1; //sto inserendo un altro blocco!
            to_add_last.file_blocks[idx] = idx_block_tmp; //salvo idx file ma devo fare update su disco
            memcpy(buffer_tmp, &to_add_last, sizeof(DirectoryBlock));
            res = DiskDriver_updateBlock(d->sfs->disk, buffer_tmp, d->current_block->next_block); //update of my dir block created with inserting of file
            if(res < 0){
                printf("Error when updating new block of directory\n\n");
                exit(-1);
            }
        }
        else{//aggiungo soltanto all'ultimo blocco e modifico il dir block con updateblock
            db_tmp->file_blocks[idx] = idx_block_tmp; //idx block temp quello del file
        }

        if(db_tmp != NULL){//salvo modifiche ultimo blocco se e solo se non ho lavorato al primo blocco come ultimo, perchè tanto quello lo aggiorno prima di creare file
            //salvo modifiche fatte al db_tmp
            res = DiskDriver_updateBlock(d->sfs->disk, read_block_dir_tmp, d->pos_in_block);
            if(res < 0){
                printf("Error when updating last block of directory\n\n");
                exit(-1);
            }
        }
        
    }

    d->current_block = &(d->dcb->header);
    d->pos_in_block = d->dcb->fcb.block_in_disk;

    //salvo modifiche al first directory block
    d->dcb->num_entries += 1;
    memcpy(buffer_tmp, d->dcb, sizeof(FirstDirectoryBlock));
    res = DiskDriver_updateBlock(d->sfs->disk, buffer_tmp, d->dcb->fcb.block_in_disk);
    if(res < 0){
        printf("Error when updating first block of directory\n\n");
        exit(-1);
    }

    FirstFileBlock ffb_tmp = {0};
    ffb_tmp.header.previous_block = -1;
    ffb_tmp.header.next_block = -1;
    ffb_tmp.header.block_in_file = 0;
    ffb_tmp.fcb.directory_block = d->dcb->fcb.block_in_disk;
    ffb_tmp.fcb.block_in_disk = idx_block_tmp;
    ffb_tmp.fcb.size_in_bytes = 0;
    ffb_tmp.fcb.size_in_blocks = 1;
    ffb_tmp.fcb.is_dir = 0;
        
    memcpy(&(ffb_tmp.fcb.name), filename ,strlen(filename));
    char ffb_block[BLOCK_SIZE] = {0};
    memcpy(ffb_block, &ffb_tmp, sizeof(FirstFileBlock));
    
    res = DiskDriver_writeBlock(d->sfs->disk, ffb_block, ffb_tmp.fcb.block_in_disk);
    if(res < 0){
        printf("Error when writing the first block of file created\n\n");
        exit(-1);
    } 

    FileHandle* to_ret = (FileHandle*)malloc(sizeof(FileHandle));
    to_ret->fcb = (FirstFileBlock*)malloc(sizeof(FirstFileBlock));
    to_ret->p_directory = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));

    to_ret->sfs = d->sfs;
    memcpy(to_ret->fcb, &ffb_tmp, sizeof(FirstFileBlock));
    memcpy(to_ret->p_directory, d->dcb, sizeof(FirstDirectoryBlock));
    to_ret->current_block = &(to_ret->fcb->header);
    to_ret->pos_in_file = 0;

    return to_ret;
}

int SimpleFS_readDir(char** names, DirectoryHandle* d){
    int idx_block_file, res, cnt = 0;
    int idx_max_fb = (BLOCK_SIZE
		   -sizeof(BlockHeader)
		   -sizeof(FileControlBlock)
		    -sizeof(int))/sizeof(int); //87
    
    char read_block_file_tmp[BLOCK_SIZE] = {0};
    for(; cnt < idx_max_fb; cnt++){
        idx_block_file = d->dcb->file_blocks[cnt];
        if(idx_block_file == 0) break;
        res = DiskDriver_readBlock(d->sfs->disk, read_block_file_tmp, idx_block_file);
        if(res < 0){
            printf("Error when reading fb of file in readDir\n\n");
            exit(-1);
        }
        FirstFileBlock* ffb_tmp = (FirstFileBlock*)read_block_file_tmp;
        memcpy(names[cnt], ffb_tmp->fcb.name, 128);
    }

    char read_block_dir_tmp[BLOCK_SIZE] = {0};
    while(cnt < d->dcb->num_entries){
        res = DiskDriver_readBlock(d->sfs->disk, read_block_dir_tmp, d->current_block->next_block);
        if(res < 0){
            printf("Error when reading other block of directory in readDir\n\n");
            exit(-1);
        }
        DirectoryBlock* db_tmp = (DirectoryBlock*)read_block_dir_tmp;
        d->current_block = &(db_tmp->header);
        idx_max_fb = (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int);
        for(int i=0; i < idx_max_fb; i++){
            idx_block_file = db_tmp->file_blocks[i];
            if(idx_block_file == 0) break;
            res = DiskDriver_readBlock(d->sfs->disk, read_block_file_tmp, idx_block_file);
            if(res < 0){
                printf("Error when reading fb of file in readDir 2\n\n");
                exit(-1);
            }
            FirstFileBlock* ffb_tmp = (FirstFileBlock*)read_block_file_tmp;
            memcpy(names[cnt], ffb_tmp->fcb.name, 128);
            cnt++;
        }
    }

    d->current_block = &(d->dcb->header);

    return 0;
}

FileHandle* SimpleFS_openFile(DirectoryHandle* d, const char* filename){
    char found = 0;
    int idx_block_file, res, cnt = 0;
    int idx_max_fb = (BLOCK_SIZE
		   -sizeof(BlockHeader)
		   -sizeof(FileControlBlock)
		    -sizeof(int))/sizeof(int); //87
    
    FirstFileBlock* ffb_tmp;
    char read_block_file_tmp[BLOCK_SIZE] = {0};
    for(; cnt < idx_max_fb; cnt++){
        idx_block_file = d->dcb->file_blocks[cnt];
        if(idx_block_file == 0) break;
        res = DiskDriver_readBlock(d->sfs->disk, read_block_file_tmp, idx_block_file);
        if(res < 0){
            printf("Error when reading fb of file in openFile\n\n");
            exit(-1);
        }
        ffb_tmp = (FirstFileBlock*)read_block_file_tmp;
        if(strcmp(ffb_tmp->fcb.name,filename) == 0 && ffb_tmp->fcb.is_dir == 0){
            found = 1;
            cnt = d->dcb->num_entries;
            break;
        }
    }
    
    char read_block_dir_tmp[BLOCK_SIZE] = {0};
    while(cnt < d->dcb->num_entries){
        res = DiskDriver_readBlock(d->sfs->disk, read_block_dir_tmp, d->current_block->next_block);
        if(res < 0){
            printf("Error when reading other block of directory in openFile\n\n");
            exit(-1);
        }
        DirectoryBlock* db_tmp = (DirectoryBlock*)read_block_dir_tmp;
        d->current_block = &(db_tmp->header);
        idx_max_fb = (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int);
        for(int i=0; i < idx_max_fb; i++){
            idx_block_file = db_tmp->file_blocks[i];
            if(idx_block_file == 0) break;
            res = DiskDriver_readBlock(d->sfs->disk, read_block_file_tmp, idx_block_file);
            if(res < 0){
                printf("Error when reading fb of file in openFile 2\n\n");
                exit(-1);
            }
            ffb_tmp = (FirstFileBlock*)read_block_file_tmp;
            cnt++;            
            if(strcmp(ffb_tmp->fcb.name,filename) == 0 && ffb_tmp->fcb.is_dir == 0){
                found = 1;
                cnt = d->dcb->num_entries;
                break;
            }
        }
    }

    FileHandle* to_ret = NULL;
    if(found){
        to_ret = (FileHandle*)malloc(sizeof(FileHandle));
        to_ret->fcb = (FirstFileBlock*)malloc(sizeof(FirstFileBlock));
        to_ret->p_directory = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));

        to_ret->sfs = d->sfs;
        memcpy(to_ret->fcb, ffb_tmp, sizeof(FirstFileBlock));
        memcpy(to_ret->p_directory, d->dcb, sizeof(FirstDirectoryBlock));
        to_ret->current_block = &(to_ret->fcb->header);
        to_ret->pos_in_file = 0;
    }

    return to_ret;

}

int SimpleFS_close(FileHandle* f){
    if(!f) return 0;
    free(f->p_directory);
    free(f->fcb);
    free(f);

    return 0;
}

int SimpleFS_write(FileHandle* f, void* data, int size){
    int toWrite,idx_byte_file,next,res, cnt = 0;
    int offset = 0;

    if(f == NULL || data == NULL || size < 0){
        printf("Error in parameters of writeFile\n\n");
        return -1;
    }

    int max_ffb = BLOCK_SIZE-sizeof(FileControlBlock) - sizeof(BlockHeader);
    int max_fb = BLOCK_SIZE-sizeof(BlockHeader);

    
    void* aux = data;  
    char read_block_file_tmp[BLOCK_SIZE] = {0};
    FileBlock* fb_tmp = NULL;
    
    //choosing where to start writing
    if(f->pos_in_file < max_ffb){ 
        idx_byte_file = f->pos_in_file; //pos where to start
        toWrite = (max_ffb - idx_byte_file)<=size?(max_ffb - idx_byte_file):size; //to write in the first bock
        memcpy((f->fcb->data + idx_byte_file),aux,toWrite);
        cnt += toWrite; //counter of wrote bytes
        aux += toWrite;
        next = f->current_block->next_block;
        if(next > 0 && cnt < size){
            res = DiskDriver_readBlock(f->sfs->disk,read_block_file_tmp,next);
            if(res < 0){
                printf("Error when reading 2nd block of file\n\n");
                return -1;
            }
        }
        if(cnt == (max_ffb - idx_byte_file) && next < 0 && f->sfs->disk->header->free_blocks > 0){
            next = DiskDriver_getFreeBlock(f->sfs->disk,f->sfs->disk->header->first_free_block);
            if(next < 0){
                printf("Error when getting free block from disk driver in writeFile\n\n");
                return -1;
            }
            FileBlock tmp = {0};
            tmp.header.block_in_file = f->fcb->fcb.size_in_blocks;
            tmp.header.previous_block = f->fcb->fcb.block_in_disk;
            tmp.header.next_block = -1;
            f->fcb->header.next_block = next;
            f->fcb->fcb.size_in_blocks += 1;
            memcpy(read_block_file_tmp,&tmp,sizeof(FileBlock));
            res = DiskDriver_writeBlock(f->sfs->disk,read_block_file_tmp,f->fcb->header.next_block);
            if(res < 0){
                printf("Error when writing last block of file\n\n");
                return -1;
            }
        }
        fb_tmp = (FileBlock*) read_block_file_tmp;
    }
    else{
        idx_byte_file = f->pos_in_file - max_ffb;
        offset = (idx_byte_file % max_fb); //questa era sotto la divisione e usciva sempre 0
        idx_byte_file = (idx_byte_file / max_fb) + 1;
        for(int i=0; i < idx_byte_file;i++){
            next = f->current_block->next_block;
            res = DiskDriver_readBlock(f->sfs->disk,read_block_file_tmp,next);
            if(res < 0){
                printf("Error when reading blocks of file in writeFile\n\n");
                return -1;
            }
            fb_tmp = (FileBlock*)read_block_file_tmp;
            f->current_block = &(fb_tmp->header);
        }
    }
    //from here we'll write from fb_tmp + offset
    char buf_tmp[BLOCK_SIZE] = {0};
    while(cnt < size && next > 0){
        toWrite = (size - cnt)<(max_fb-offset)?(size - cnt):(max_fb-offset);
        memcpy((fb_tmp->data+offset),aux,toWrite);
        res = DiskDriver_updateBlock(f->sfs->disk,read_block_file_tmp,next); //next contains block in disk where we're going to write
        if(res < 0){
            printf("Error when writing a block of file\n\n");            
            return -1;
        }
        cnt += toWrite;
        aux += toWrite;
        int prev = next;
        next = fb_tmp->header.next_block;
        if(next > 0 && cnt < size){        
            res = DiskDriver_readBlock(f->sfs->disk,buf_tmp,next);
            if(res < 0){
                printf("Error when reading next block of file\n\n");
                return -1;
            }
        }
        if(toWrite == (max_fb-offset) && next < 0 && f->sfs->disk->header->free_blocks > 0){//preventiva
            next = DiskDriver_getFreeBlock(f->sfs->disk,f->sfs->disk->header->first_free_block);
            if(next < 0){
                printf("Error in getting a free block from disk driver\n\n");
                return -1;
            }
            FileBlock tmp = {0};
            tmp.header.block_in_file = f->fcb->fcb.size_in_blocks;
            tmp.header.previous_block = prev;
            tmp.header.next_block = -1;
            fb_tmp->header.next_block = next;
            f->fcb->fcb.size_in_blocks += 1;
            memcpy(buf_tmp,&tmp,sizeof(FileBlock));
            res = DiskDriver_writeBlock(f->sfs->disk,buf_tmp,fb_tmp->header.next_block);
            if(res < 0){
                printf("Error in saving a preemptive block of file\n\n");
                return -1;
            }
            res = DiskDriver_updateBlock(f->sfs->disk,read_block_file_tmp,prev);
            if(res < 0){
                printf("Error in updating previous block of preemtive's\n\n");
                return -1;
            }
        }
        memcpy(read_block_file_tmp,buf_tmp,BLOCK_SIZE);
        offset = 0;
    }

    if(f->pos_in_file + cnt > f->fcb->fcb.size_in_bytes){
        int surplus = f->pos_in_file + cnt - f->fcb->fcb.size_in_bytes;
        f->p_directory->fcb.size_in_bytes += surplus;
        f->fcb->fcb.size_in_bytes += surplus;
        DirectoryHandle* dh_tmp = SimpleFS_createDirHandle(f->p_directory, f->sfs);
        res = SimpleFS_updateSizeDir(dh_tmp, surplus, 1);
        if(res < 0) return -1;
        free(dh_tmp->p_directory);
        free(dh_tmp->dcb);
        free(dh_tmp);
    }

    f->pos_in_file += cnt;    
    f->current_block = &(f->fcb->header); //ripristino

    
    res = DiskDriver_updateBlock(f->sfs->disk,f->fcb,f->fcb->fcb.block_in_disk);
    if(res < 0){
        printf("Error when updating first block of file\n\n");
        return -1;
    }

    res = DiskDriver_updateBlock(f->sfs->disk,f->p_directory,f->p_directory->fcb.block_in_disk);
    if(res < 0){
        printf("Error when updating first block of directory of file\n\n");
        return -1;
    }
    
    return cnt;
}

int SimpleFS_read(FileHandle* f, void* data, int size){
    int toRead,idx_byte_file,next,res, cnt = 0;
    int offset = 0;

    if(f == NULL || data == NULL || size < 0 || (f->pos_in_file + size)>f->fcb->fcb.size_in_bytes){
        return -1;
    }

    int max_ffb = BLOCK_SIZE-sizeof(FileControlBlock) - sizeof(BlockHeader);
    int max_fb = BLOCK_SIZE-sizeof(BlockHeader);

    void* aux = data;  
    char read_block_file_tmp[BLOCK_SIZE] = {0};
    FileBlock* fb_tmp = NULL;

    //choosing where to start reading
    if(f->pos_in_file < max_ffb){ 
        idx_byte_file = f->pos_in_file; //pos where to start
        toRead = (max_ffb - idx_byte_file)<=size?(max_ffb - idx_byte_file):size; //to write in the first bock
        memcpy(aux, (f->fcb->data + idx_byte_file), toRead);
        cnt += toRead; //counter of wrote bytes
        aux += toRead;
        next = f->current_block->next_block;
        if(cnt < size){
            res = DiskDriver_readBlock(f->sfs->disk,read_block_file_tmp,next);
            if(res < 0){
                printf("Error in reading first block of file\n\n");
                return -1;
            }
        }
        fb_tmp = (FileBlock*) read_block_file_tmp;
    }
    else{
        idx_byte_file = f->pos_in_file - max_ffb;
        offset = (idx_byte_file % max_fb); //questa era sotto la divisione e usciva sempre 0
        idx_byte_file = (idx_byte_file / max_fb) + 1;
        for(int i=0; i<idx_byte_file; i++){
            next = f->current_block->next_block;
            res = DiskDriver_readBlock(f->sfs->disk,read_block_file_tmp,next);
            if(res < 0){
                printf("Error in reading others blocks of file\n\n");
                return -1;
            }
            fb_tmp = (FileBlock*)read_block_file_tmp;
            f->current_block = &(fb_tmp->header);
        }
    }

    //from here we'll read from fb_tmp + offset
    while(cnt < size && next > 0){
        toRead = (size - cnt)<max_fb?(size - cnt):max_fb;
        memcpy(aux, (fb_tmp->data+offset), toRead);
        res = DiskDriver_updateBlock(f->sfs->disk,read_block_file_tmp,next); //next contains block in disk where we're going to write
        if(res < 0){
            return -1;
        }
        cnt += toRead;
        aux += toRead;
        next = fb_tmp->header.next_block;
        if(cnt < size && next > 0){        
            res = DiskDriver_readBlock(f->sfs->disk,read_block_file_tmp,next);
            if(res < 0){
                printf("Error when reading next block of file\n\n");
                return -1;
            }
        }
        offset = 0;
    }

    f->pos_in_file += cnt;
    f->current_block = &(f->fcb->header); //ripristino

    return cnt;
}

int SimpleFS_seek(FileHandle* f, int pos){
    if(pos < 0 || pos > f->fcb->fcb.size_in_bytes){
        printf("File too short or index invalid\n\n");
        return -1;
    }
    f->pos_in_file = pos;
    return pos;
}

int SimpleFS_changeDir(DirectoryHandle* d, char* dirname){
    int i,res,idx_found = 0;
    char exist = 0;
    char buffer_tmp[BLOCK_SIZE] = {0};
    char dirb_tmp[BLOCK_SIZE] = {0};
    FirstDirectoryBlock* dirToRet_tmp;
    DirectoryBlock* db_tmp;

    int idx_max_ffb = (BLOCK_SIZE
	    -sizeof(BlockHeader)
		-sizeof(FileControlBlock)
		-sizeof(int))/sizeof(int);  
    int idx_max_fb = (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int);
    
    //in case you want to go to p_dir
    if(strcmp(dirname,"..") == 0){
        if(d->p_directory == NULL){ //check if it has a p_dir
            printf("No parent dir, are you on root?\n\n");
            return -1;
        }

        if(d->p_directory->fcb.block_in_disk == d->sfs->root){ //when returning to root using ".."
            memcpy(d->dcb,d->p_directory,sizeof(FirstDirectoryBlock)); 
            free(d->p_directory);
            d->p_directory = NULL;
            d->current_block = &(d->dcb->header);
            d->pos_in_block = d->sfs->root;
            d->pos_in_dir = 0;
            d->sfs->curr_dir = d->pos_in_block;
            
            return 0;
        }

        //read the parent block to assign to p_dir
        res = DiskDriver_readBlock(d->sfs->disk, buffer_tmp, d->p_directory->fcb.directory_block);
        if(res < 0){
            printf("Error when reading parent's fdb\n\n");
            return -1;
        }
        
        dirToRet_tmp = (FirstDirectoryBlock*)buffer_tmp;

        memcpy(d->dcb,d->p_directory,sizeof(FirstDirectoryBlock)); 
        memcpy(d->p_directory,dirToRet_tmp,sizeof(FirstDirectoryBlock));
        d->current_block = &(d->dcb->header);
        d->pos_in_block = d->dcb->fcb.block_in_disk;
        d->pos_in_dir = 0;
        d->sfs->curr_dir = d->pos_in_block;
        
        return 0;
    }
    //searching for the dir to access
    for(i = 0; (i < idx_max_ffb) && !exist; i++){
        idx_found = d->dcb->file_blocks[i];
        res = DiskDriver_readBlock(d->sfs->disk, buffer_tmp, idx_found);
        if(res < 0){
            printf("Error when reading first block of file/dir\n\n");
            return -1;
        }
        
        dirToRet_tmp = (FirstDirectoryBlock*)buffer_tmp;

        if(strcmp(dirToRet_tmp->fcb.name,dirname) == 0 && dirToRet_tmp->fcb.is_dir == 1){
            exist = 1;
        }
    }
    
    for(i = 1; (i < d->dcb->fcb.size_in_blocks) && !exist; i++){
        int idx_block_dirb = d->current_block->next_block;
        res = DiskDriver_readBlock(d->sfs->disk, dirb_tmp, idx_block_dirb);
        if(res < 0){
            printf("Error when reading next block of directory\n\n");
            return -1;
        }
        db_tmp = (DirectoryBlock*)dirb_tmp; //last directory block
        d->pos_in_block = idx_block_dirb; //id block n disk last db
        d->current_block = &(db_tmp->header);//block header of last db

        for(int j = 0; (j < idx_max_fb) && !exist; j++){
            idx_found = db_tmp->file_blocks[j];
            res = DiskDriver_readBlock(d->sfs->disk, buffer_tmp, idx_found);
            if(res < 0){
                printf("Error when reading first block of file\n\n");
                return -1;
            }
            
            dirToRet_tmp = (FirstDirectoryBlock*)buffer_tmp;

            if(strcmp(dirToRet_tmp->fcb.name,dirname) == 0 && dirToRet_tmp->fcb.is_dir == 1){
                exist = 1;
            }
        }
    }

    if(!exist){
        //ripristino
        d->current_block = &(d->dcb->header);
        d->pos_in_block = d->dcb->fcb.block_in_disk;
        return -1;
    }

    //from here u have the dir in dirToRet_tmp need to side effect on "d"

    if(d->p_directory == NULL){
        d->p_directory = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));
    }
    memcpy(d->p_directory,d->dcb,sizeof(FirstDirectoryBlock));
    memcpy(d->dcb,dirToRet_tmp,sizeof(FirstDirectoryBlock));
    d->current_block = &(d->dcb->header);
    d->pos_in_block = idx_found;
    d->pos_in_dir = 0;
    d->sfs->curr_dir = d->pos_in_block;
    
    return 0;
}

int SimpleFS_mkDir(DirectoryHandle* d, char* dirname){
    int i,res;

    if(d->sfs->disk->header->first_free_block < 0){
        printf("No free blocks available\n\n");
        return -1;
    };

    char** names = malloc(sizeof(char*)*d->dcb->num_entries);
    for(i=0; i<d->dcb->num_entries; i++) names[i] = malloc(128);

    SimpleFS_readDir(names, d);

    //if there is already a dir with dirname exit
    for(int i=0; i < d->dcb->num_entries; i++) {
        if(strcmp(names[i], dirname) == 0){
            printf("Directory already exist -> change name\n\n");
            for(int i=0; i<d->dcb->num_entries; i++) free(names[i]);
            free(names);
            return -1;
        }
    }

    for(int i=0; i<d->dcb->num_entries; i++) free(names[i]);
    free(names);

    int idx_block_tmp = DiskDriver_getFreeBlock(d->sfs->disk,d->sfs->disk->header->first_free_block);
    int idx_max_ffb = (BLOCK_SIZE
	    -sizeof(BlockHeader)
		-sizeof(FileControlBlock)
		-sizeof(int))/sizeof(int);  
   
    
    char buffer_tmp[BLOCK_SIZE] = {0};

    if(d->dcb->num_entries+1 <= idx_max_ffb){
        //entra nel primo blocco
        d->dcb->file_blocks[d->dcb->num_entries] = idx_block_tmp;  
    }
    else{
        int idx_max_fb = (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int); //max block for dir block (not first)
        int idx = (d->dcb->num_entries - idx_max_ffb);
        idx = idx % idx_max_fb; //if 0 create a dir block oterwhise add in last dir block      

        char read_block_dir_tmp[BLOCK_SIZE] = {0};
        DirectoryBlock* db_tmp = NULL;
        for(i = 1; i < d->dcb->fcb.size_in_blocks; i++){
            int idx_block_last_dirb = d->current_block->next_block;
            res = DiskDriver_readBlock(d->sfs->disk, read_block_dir_tmp, idx_block_last_dirb);
            if(res < 0){
                printf("Error when reading last block of directory\n\n");
                return -1;
            }
            db_tmp = (DirectoryBlock*)read_block_dir_tmp; //last directory block
            d->pos_in_block = idx_block_last_dirb; //id block n disk last db
            d->current_block = &(db_tmp->header);//block header of last db
        }

        if(idx == 0){//dovrò creare un dir block e collegarlo (uso idx_tmp e poi richiedo getfreeblock per file)
            DirectoryBlock to_add_last = {0};
            to_add_last.header.next_block = -1; //its last
            to_add_last.header.block_in_file = d->current_block->block_in_file + 1; //aggiungo un blocco
            to_add_last.header.previous_block = d->pos_in_block; //il pred è il blocco ex last
            memset(&(to_add_last.file_blocks), 0, idx_max_fb); //non serve manco
            
            d->current_block->next_block = idx_block_tmp; //lo rubo al file
            
            //devo salvare modifiche dei dir block creato
            memcpy(buffer_tmp, &to_add_last, sizeof(DirectoryBlock));
            res = DiskDriver_writeBlock(d->sfs->disk, buffer_tmp, idx_block_tmp);
            if(res < 0){
                printf("Error when writing new block of directory\n\n");
                return -1;
            }

            if(d->sfs->disk->header->first_free_block < 0){ //non posso aggiungerci il file
                res = DiskDriver_freeBlock(d->sfs->disk, idx_block_tmp);
                if(res < 0){
                    printf("Error when freeing the new block of directory\n\n");
                    return -1;
                }
                printf("No free blocks available for dir -> deleting folder's update\n\n");
                return -1;
            };
            idx_block_tmp = DiskDriver_getFreeBlock(d->sfs->disk, d->sfs->disk->header->first_free_block);

            d->dcb->fcb.size_in_blocks += 1; //sto inserendo un altro blocco!
            to_add_last.file_blocks[idx] = idx_block_tmp; //salvo idx file ma devo fare update su disco
            memcpy(buffer_tmp, &to_add_last, sizeof(DirectoryBlock));
            res = DiskDriver_updateBlock(d->sfs->disk, buffer_tmp, d->current_block->next_block); //update of my dir block created with inserting of file
            if(res < 0){
                printf("Error when updating the new block of directory with adding a new entry\n\n");
                return -1;
            }
        }
        else{//aggiungo soltanto all'ultimo blocco e modifico il dir block con updateblock
            db_tmp->file_blocks[idx] = idx_block_tmp; //idx block temp quello del file
        }

        if(db_tmp != NULL){//salvo modifiche ultimo blocco se e solo se non ho lavorato al primo blocco come ultimo, perchè tanto quello lo aggiorno prima di creare file
            //salvo modifiche fatte al db_tmp
            res = DiskDriver_updateBlock(d->sfs->disk, read_block_dir_tmp, d->pos_in_block);
            if(res < 0){
                printf("Error when updating a block of directory\n\n");
                return -1;
            }
        }
        
    }

    d->current_block = &(d->dcb->header);
    d->pos_in_block = d->dcb->fcb.block_in_disk;

    //salvo modifiche al first directory block
    d->dcb->num_entries += 1;
    memcpy(buffer_tmp, d->dcb, sizeof(FirstDirectoryBlock));
    res = DiskDriver_updateBlock(d->sfs->disk, buffer_tmp, d->dcb->fcb.block_in_disk);
    if(res < 0){
        printf("Error when updating first block of directory\n\n");
        return -1;
    }

    FirstDirectoryBlock fdb_tmp = {0};
    fdb_tmp.header.previous_block = -1;
    fdb_tmp.header.next_block = -1;
    fdb_tmp.header.block_in_file = 0;
    fdb_tmp.fcb.directory_block = d->dcb->fcb.block_in_disk;
    fdb_tmp.fcb.block_in_disk = idx_block_tmp;
    fdb_tmp.fcb.size_in_bytes = 0;
    fdb_tmp.fcb.size_in_blocks = 1;
    fdb_tmp.fcb.is_dir = 1;
        
    memcpy(&(fdb_tmp.fcb.name), dirname, strlen(dirname));
    char fdb_block[BLOCK_SIZE] = {0};
    memcpy(fdb_block, &fdb_tmp, sizeof(FirstDirectoryBlock));
    
    res = DiskDriver_writeBlock(d->sfs->disk, fdb_block, fdb_tmp.fcb.block_in_disk);
    if(res < 0){
        printf("Error when writing first block of new directory created\n\n");
        return -1;
    } 

    return 0;
}

int SimpleFS_updateSizeDir(DirectoryHandle* d, int size_in_bytes_torem, int addOrRemove){
    int res;
    int tmp[BLOCK_SIZE] = {0};

    if(d->p_directory == NULL) return 0;
    if(addOrRemove == 0) d->p_directory->fcb.size_in_bytes -= size_in_bytes_torem; // aggiorno padre e vado a prendere gli altri padri senza cambiare dcb
    else d->p_directory->fcb.size_in_bytes += size_in_bytes_torem;
    memcpy(tmp, d->p_directory, sizeof(FirstDirectoryBlock));
    res = DiskDriver_updateBlock(d->sfs->disk, tmp, d->p_directory->fcb.block_in_disk);
    if(res < 0) return -1;

    while(d->p_directory->fcb.directory_block > 0){
        res = DiskDriver_readBlock(d->sfs->disk, tmp, d->p_directory->fcb.directory_block);
        if(res < 0) return -1;
        FirstDirectoryBlock* p_tmp = (FirstDirectoryBlock*)tmp;
        if(addOrRemove == 0) p_tmp->fcb.size_in_bytes -= size_in_bytes_torem;
        else p_tmp->fcb.size_in_bytes += size_in_bytes_torem;
        res = DiskDriver_updateBlock(d->sfs->disk, tmp, d->p_directory->fcb.directory_block);
        if(res < 0) return -1;
        memcpy(d->p_directory, p_tmp, sizeof(FirstDirectoryBlock));
    }

    return 0;
}

int SimpleFS_remove(DirectoryHandle* d, char* filename){
    int res, i, idx_found, idx_entry, size_in_bytes_torem;
    char exist = 0;

    int idx_max_ffb = (BLOCK_SIZE
	    -sizeof(BlockHeader)
		-sizeof(FileControlBlock)
		-sizeof(int))/sizeof(int);  
    int idx_max_fb = (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int);

    char buffer_tmp[BLOCK_SIZE] = {0};
    char dirb_tmp[BLOCK_SIZE] = {0};
    FirstFileBlock* file_tmp;
    DirectoryBlock* db_tmp = NULL;

    for(i = 0; (i < idx_max_ffb) && !exist; i++){
        idx_found = d->dcb->file_blocks[i];
        res = DiskDriver_readBlock(d->sfs->disk, buffer_tmp, idx_found);
        if(res < 0){
            printf("Error when reading first block of file\n\n");
            return -1;
        }
        
        file_tmp = (FirstFileBlock*)buffer_tmp;
        if(strcmp(file_tmp->fcb.name,filename) == 0){
            exist = 1;
            idx_entry = i;
        }
    }
    
    for(i = 1; (i < d->dcb->fcb.size_in_blocks) && !exist; i++){
        int idx_block_dirb = d->current_block->next_block;
        res = DiskDriver_readBlock(d->sfs->disk, dirb_tmp, idx_block_dirb);
        if(res < 0){
            printf("Error when reading others block of directory\n\n");
            return -1;
        }
        db_tmp = (DirectoryBlock*)dirb_tmp; //last directory block
        d->pos_in_block = idx_block_dirb; //id block n disk last db
        d->current_block = &(db_tmp->header);//block header of last db

        for(int j = 0; (j < idx_max_fb) && !exist; j++){
            idx_found = db_tmp->file_blocks[j];
            res = DiskDriver_readBlock(d->sfs->disk, buffer_tmp, idx_found);
            if(res < 0){
                printf("Error when reading first block of file\n\n");
                return -1;
            }
            
            file_tmp = (FirstFileBlock*)buffer_tmp;

            if(strcmp(file_tmp->fcb.name,filename) == 0){
                exist = 1;
                idx_entry = j;
            }
        }
    }

    int idx_block_db_of_file = d->pos_in_block;

    //ripristino
    d->current_block = &(d->dcb->header);
    d->pos_in_block = d->dcb->fcb.block_in_disk;

    if(!exist){
        printf("File or dir not exist\n\n");
        return -1;
    }

    //se sono qui esiste sicuro

    //devo aggiornare size cartella corrente e quella dei padri fino a '/'
    size_in_bytes_torem = file_tmp->fcb.size_in_bytes;
    d->dcb->fcb.size_in_bytes -= size_in_bytes_torem;
    SimpleFS_updateSizeDir(d, size_in_bytes_torem, 0);

    if(file_tmp->fcb.is_dir) {
        FirstDirectoryBlock* fdb_tmp = (FirstDirectoryBlock*)file_tmp;
        int num_entries = fdb_tmp->num_entries;

        char** names = malloc(sizeof(char*)*num_entries);
        for(i=0; i<num_entries; i++) names[i] = malloc(128);

        //creo directory handler per scendere nella cartella e cancellare il contenuto ricorsivamente
        DirectoryHandle* dh_rec = (DirectoryHandle*)malloc(sizeof(DirectoryHandle));
        dh_rec->dcb = fdb_tmp;
        dh_rec->p_directory = NULL;
        dh_rec->current_block = &(dh_rec->dcb->header);
        dh_rec->pos_in_dir = 0;
        dh_rec->sfs = d->sfs;
        dh_rec->pos_in_block = dh_rec->dcb->fcb.block_in_disk;

        SimpleFS_readDir(names, dh_rec);

        for(i = 0; i < num_entries; i++) {
            SimpleFS_remove(dh_rec, names[i]);
        }

        for(i=0; i<num_entries; i++) free(names[i]);
        free(names);
        free(dh_rec);
        //ricorsivamente cancellato il contenuto
    }

    DirectoryBlock* last_db;
    char dir_buf[BLOCK_SIZE] = {0}; //è il buffer dell'ultimo blocco 
    char support_dir_update_buf[BLOCK_SIZE] = {0};
    int idx_block_last_dirb;
    int num_entries_last_block = ((d->dcb->num_entries - idx_max_ffb) % idx_max_fb) == 0 ? idx_max_fb:((d->dcb->num_entries - idx_max_ffb) % idx_max_fb);

    //cancellare file o cartella(esterna ormai vuoto)
    if(d->dcb->num_entries <= idx_max_ffb){//Ho solo il first dir block (OK)
        if((d->dcb->num_entries == 1) || (d->dcb->num_entries - 1 == idx_entry)){
            d->dcb->file_blocks[idx_entry] = 0; //ho solo una entry da eliminare (o in prima o in ultima pos)
        } 
        else{
            //caso in cui elimino un entry non ultima con più di una entries nell'array
            d->dcb->file_blocks[idx_entry] = d->dcb->file_blocks[d->dcb->num_entries - 1];
            d->dcb->file_blocks[d->dcb->num_entries - 1] = 0;
        }
    }
    else if(db_tmp != NULL && db_tmp->header.next_block < 0){//caso in cui file in ultimo blocco (OK)
        if(num_entries_last_block == 1){//devo eliminare direttamente tutto il blocco
            int idx_to_delete = idx_block_db_of_file;
            if(db_tmp->header.previous_block == d->dcb->fcb.block_in_disk){
                d->dcb->header.next_block = -1;
            }
            else{
                res = DiskDriver_readBlock(d->sfs->disk, support_dir_update_buf, db_tmp->header.previous_block);
                if(res < 0){
                    printf("Error when reading previous block of directory\n\n");
                    return -1;
                }
                DirectoryBlock* support_p = (DirectoryBlock*)support_dir_update_buf;
                support_p->header.next_block = -1;
                res = DiskDriver_updateBlock(d->sfs->disk, support_dir_update_buf, db_tmp->header.previous_block); //ho aggiornato il prev di quello da elim
                if(res < 0){
                    printf("Error when updating previous block of directory\n\n");
                    return -1;
                }
            }
            d->dcb->fcb.size_in_blocks -= 1; //elimino un pezzo            
            res = DiskDriver_freeBlock(d->sfs->disk, idx_to_delete);
            if(res < 0){
                printf("Error when freeing block to delete of directory\n\n");
                return -1;
            }
        }
        else if(idx_entry == num_entries_last_block -1) {
            db_tmp->file_blocks[idx_entry] = 0; //ultimo elemento in assoluto, lo metto semplicemente a 0
            res = DiskDriver_updateBlock(d->sfs->disk, dirb_tmp, idx_block_db_of_file);
            if(res < 0){
                printf("Error when updating block with file found");
                return -1;
            }
        }
        else{
            db_tmp->file_blocks[idx_entry] = db_tmp->file_blocks[num_entries_last_block - 1];
            db_tmp->file_blocks[num_entries_last_block - 1] = 0;
            res = DiskDriver_updateBlock(d->sfs->disk, dirb_tmp, idx_block_db_of_file);
            if(res < 0){
                printf("Error when updating block with file found");
                return -1;
            }
        }
    }
    else{//caso in cui è in un blocco intermedio
        int idx_to_delete;
        for(i = 1; i < d->dcb->fcb.size_in_blocks; i++){
            idx_block_last_dirb = d->current_block->next_block;
            res = DiskDriver_readBlock(d->sfs->disk, dir_buf, idx_block_last_dirb);
            if(res < 0){
                printf("Error when reading last block of directory\n\n");
                return -1;
            }
            last_db = (DirectoryBlock*)dir_buf; //last directory block
            d->pos_in_block = idx_block_last_dirb; //id block n disk last db
            d->current_block = &(last_db->header);//block header of last db
        }
        //ripristino
        d->current_block = &(d->dcb->header);
        d->pos_in_block = d->dcb->fcb.block_in_disk;

        //last_db sarà ultimo SOLO QUESTO DA FARE
        if(num_entries_last_block == 1){//devo eliminare direttamente tutto il blocco ultimo
            if(idx_block_db_of_file == d->dcb->fcb.block_in_disk){
                d->dcb->file_blocks[idx_entry] = last_db->file_blocks[num_entries_last_block - 1];
            }
            else{
                db_tmp->file_blocks[idx_entry] = last_db->file_blocks[num_entries_last_block - 1];
                res = DiskDriver_updateBlock(d->sfs->disk, dirb_tmp, idx_block_db_of_file); //ho aggiornato il prev di quello da elim
                if(res < 0){
                    printf("Error when updating last block of directory\n\n");
                    return -1;
                }
            }
            last_db->file_blocks[num_entries_last_block - 1] = 0; //superfluo        
            idx_to_delete = idx_block_last_dirb;
            if(last_db->header.previous_block == d->dcb->fcb.block_in_disk){//il prev è dcb
                d->dcb->header.next_block = -1;
            }
            else{
                if(last_db->header.previous_block == idx_block_db_of_file){
                    db_tmp->header.next_block = -1;
                    res = DiskDriver_updateBlock(d->sfs->disk, dirb_tmp, idx_block_db_of_file); //ho aggiornato il prev di quello da elim
                    if(res < 0){
                        printf("Error when updating last block of directory\n\n");
                        return -1;
                    }
                }
                else{
                    res = DiskDriver_readBlock(d->sfs->disk, support_dir_update_buf, last_db->header.previous_block);
                    if(res < 0){
                        printf("Error when reading previous block of directory\n\n");
                        return -1;
                    }
                    DirectoryBlock* support_p = (DirectoryBlock*)support_dir_update_buf;
                    support_p->header.next_block = -1;
                    res = DiskDriver_updateBlock(d->sfs->disk, support_dir_update_buf, last_db->header.previous_block); //ho aggiornato il prev di quello da elim
                    if(res < 0){
                        printf("Error when updating previous block of directory\n\n");
                        return -1;
                    }
                }
                
            }
            d->dcb->fcb.size_in_blocks -= 1; //elimino un pezzo
            res = DiskDriver_freeBlock(d->sfs->disk, idx_to_delete);
            if(res < 0){
                printf("Error fwhen freeing the block of directory to delete\n\n");
                return -1;
            }
        }
        else{//aggiorno prev e elimino ultimo e aggiorno dcb
            if(idx_block_db_of_file == d->dcb->fcb.block_in_disk){
                d->dcb->file_blocks[idx_entry] = last_db->file_blocks[num_entries_last_block - 1];
            }
            else{
                db_tmp->file_blocks[idx_entry] = last_db->file_blocks[num_entries_last_block - 1];
                res = DiskDriver_updateBlock(d->sfs->disk, dirb_tmp, idx_block_db_of_file); //ho aggiornato il prev di quello da elim
                if(res < 0){
                    printf("Error when updating a block of a directory\n\n");
                    return -1;
                }
            }
            last_db->file_blocks[num_entries_last_block - 1] = 0;
            //modifiche all'ultimo blocco
            res = DiskDriver_updateBlock(d->sfs->disk, dir_buf, idx_block_last_dirb);
            if(res < 0){
                printf("Error when updating a block of a directory\n\n");
                return -1;
            }
        }
    }
    //salvare modifiche struct d->dcb (entries -= 1)
    d->dcb->num_entries -= 1;
    memcpy(dir_buf, d->dcb, sizeof(FirstDirectoryBlock));
    res = DiskDriver_updateBlock(d->sfs->disk, dir_buf, d->pos_in_block);
    if(res < 0){
        printf("Error when updating first block of directory\n\n");
        return -1;
    }

    //libero tutti i blocchi che occupa un file
    BitMap bmap = {0};
    bmap.entries = d->sfs->disk->bitmap_data;
    bmap.num_bits = d->sfs->disk->header->num_blocks;

    FileBlock* fb_tmp;
    int next = file_tmp->header.next_block;

    BitMap_set(&bmap, file_tmp->fcb.block_in_disk, 0);
    d->sfs->disk->header->free_blocks += 1;
    if(d->sfs->disk->header->first_free_block < 0) d->sfs->disk->header->first_free_block = file_tmp->fcb.block_in_disk;
    else d->sfs->disk->header->first_free_block = file_tmp->fcb.block_in_disk < d->sfs->disk->header->first_free_block ? file_tmp->fcb.block_in_disk:d->sfs->disk->header->first_free_block;

    while(next > 0){
        res = DiskDriver_readBlock(d->sfs->disk, buffer_tmp, next);
        if(res < 0){
            printf("Error when reading next block of file/dir to delete\n\n");
            return -1;
        }

        fb_tmp = (FileBlock*)buffer_tmp;

        BitMap_set(&bmap, next, 0);
        d->sfs->disk->header->free_blocks += 1;
        d->sfs->disk->header->first_free_block = next < d->sfs->disk->header->first_free_block ? next:d->sfs->disk->header->first_free_block;

        next = fb_tmp->header.next_block;
    }

    return 0;
}

int SimpleFS_syncCurrDir(DirectoryHandle* d){
    int res;
    int tmp[BLOCK_SIZE] = {0};

    res = DiskDriver_readBlock(d->sfs->disk, tmp, d->dcb->fcb.block_in_disk);
    if(res < 0) return -1;
    memcpy(d->dcb, tmp, BLOCK_SIZE);

    if(d->p_directory != NULL){
        res = DiskDriver_readBlock(d->sfs->disk, tmp, d->p_directory->fcb.block_in_disk);
        if(res < 0) return -1;
        memcpy(d->p_directory, tmp, BLOCK_SIZE);
    }
    

    return 0;
}

DirectoryHandle* SimpleFS_createDirHandle(FirstDirectoryBlock* fdb, SimpleFS* fs){

    DirectoryHandle* to_ret = (DirectoryHandle*)malloc(sizeof(DirectoryHandle));
    to_ret->dcb = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));

    if(fdb->fcb.directory_block < 0) to_ret->p_directory = NULL;
    else{
        to_ret->p_directory = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));
        int buf_tmp[BLOCK_SIZE] = {0};
        int res = DiskDriver_readBlock(fs->disk, buf_tmp, fdb->fcb.directory_block);
        if(res < 0) return NULL;
        memcpy(to_ret->p_directory, buf_tmp, sizeof(FirstDirectoryBlock));
    } 

    memcpy(to_ret->dcb, fdb, sizeof(FirstDirectoryBlock));
    to_ret->current_block = &(to_ret->dcb->header);
    to_ret->sfs = fs;
    to_ret->pos_in_block = fdb->fcb.block_in_disk;
    to_ret->pos_in_dir = 0;
    return to_ret;
}

void printDir(DirectoryHandle* d){
    printf("Situation of dir '%s' -->\n", d->dcb->fcb.name);
    printf(" FS Address: %p\n FDB Address: %p\n FDB_P: %p\n CURRENT_BLOCK: %p\n POS_IN_DIR: %d\n POS_IN_BLOCK: %d\n",
            d->sfs, d->dcb, d->p_directory, d->current_block, d->pos_in_dir, d->pos_in_block);
    printf(" DISK Address: %p\n IDX_ROOT: %d\n IDX_CURR_DIR: %d\n",
            d->sfs->disk, d->sfs->root, d->sfs->curr_dir);
    printf(" NAME: %s\n IDX_FDB_PARENT: %d\n BLOCK_IN_DISK: %d\n SIZE_IN_BYTES: %d\n SIZE_IN_BLOCKS: %d\n IS_DIR: %d\n",
            d->dcb->fcb.name, d->dcb->fcb.directory_block, d->dcb->fcb.block_in_disk, d->dcb->fcb.size_in_bytes, d->dcb->fcb.size_in_blocks, d->dcb->fcb.is_dir);
    printf(" IDX_PREVIOUS: %d\n IDX_NEXT: %d\n BLOCK_IN_FILE(0 IF FCB): %d\n",
            d->dcb->header.previous_block, d->dcb->header.next_block, d->dcb->header.block_in_file);
    printf(" NUM_ENTRIES_TOT: %d\n", d->dcb->num_entries);

    if(d->dcb->num_entries > 0){
        printf("\nFile in fdb of dir -->\n");

        int idx_block_file, res, cnt = 0;
        int idx_max_fb = (BLOCK_SIZE
            -sizeof(BlockHeader)
            -sizeof(FileControlBlock)
                -sizeof(int))/sizeof(int); //87
        
        for(; cnt < idx_max_fb; cnt++){
            idx_block_file = d->dcb->file_blocks[cnt];
            if(idx_block_file == 0) break;
            printf(" File in entry n. %d, in idx_block_of_file: %d\n", cnt, idx_block_file);
        }

        char read_block_dir_tmp[BLOCK_SIZE] = {0};
        idx_max_fb = (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int);

        while(cnt < d->dcb->num_entries){
            printf("\n File in block n. %d -->\n", d->current_block->next_block);
            res = DiskDriver_readBlock(d->sfs->disk, read_block_dir_tmp, d->current_block->next_block);
            if(res < 0){
                printf("Error in read: SimpleFs_readDir 2\n\n");
                exit(-1);
            }
            DirectoryBlock* db_tmp = (DirectoryBlock*)read_block_dir_tmp;
            d->current_block = &(db_tmp->header);
            
            for(int i=0; i < idx_max_fb; i++){
                idx_block_file = db_tmp->file_blocks[i];
                if(idx_block_file == 0) break;
                printf(" File in entry n. %d, in idx_block_of_file: %d\n", i, idx_block_file);
                cnt++;
            }

            printf("\n IDX_PREVIOUS: %d\n IDX_NEXT: %d\n BLOCK_IN_FILE(0 IF FCB): %d\n",
                    db_tmp->header.previous_block, db_tmp->header.next_block, db_tmp->header.block_in_file);
        }
    }
    d->current_block = &(d->dcb->header);
}

void printFile(FileHandle* f){
    printf("Situation of file: '%s'-->\n", f->fcb->fcb.name);
    printf(" FS Address: %p\n FFB Address: %p\n FDB_P: %p\n CURRENT_BLOCK: %p\n POS_IN_FILE: %d\n",
            f->sfs, f->fcb, f->p_directory, f->current_block, f->pos_in_file);
    printf(" DISK Address: %p\n IDX_ROOT: %d\n IDX_CURR_DIR: %d\n",
            f->sfs->disk, f->sfs->root, f->sfs->curr_dir);
    printf(" NAME: %s\n IDX_FDB_PARENT: %d\n BLOCK_IN_DISK: %d\n SIZE_IN_BYTES: %d\n SIZE_IN_BLOCKS: %d\n IS_DIR: %d\n",
            f->fcb->fcb.name, f->fcb->fcb.directory_block, f->fcb->fcb.block_in_disk, f->fcb->fcb.size_in_bytes, f->fcb->fcb.size_in_blocks, f->fcb->fcb.is_dir);
    printf(" IDX_PREVIOUS: %d\n IDX_NEXT: %d\n BLOCK_IN_FILE(0 IF FCB): %d\n",
            f->fcb->header.previous_block, f->fcb->header.next_block, f->fcb->header.block_in_file);

    if(f->fcb->fcb.size_in_bytes == 0){
        printf("File is empty!\n\n");
        return;
    }
    int saved_prev_pos = f->pos_in_file;
    SimpleFS_seek(f, 0);

    char buf1[BLOCK_SIZE-sizeof(FileControlBlock) - sizeof(BlockHeader) + 1];
    memset(buf1, 0, BLOCK_SIZE-sizeof(FileControlBlock) - sizeof(BlockHeader) + 1);
    char bufelse[BLOCK_SIZE  - sizeof(BlockHeader) + 1];
    memset(bufelse, 0, BLOCK_SIZE - sizeof(BlockHeader) + 1);


    SimpleFS_read(f, buf1, BLOCK_SIZE-sizeof(FileControlBlock) - sizeof(BlockHeader));
    printf("\nData in ffb of file -->\n");
    printf("%s\n", buf1);

    char tmp[BLOCK_SIZE] = {0};
    for(int i=0; i<f->fcb->fcb.size_in_blocks - 1; i++){
        printf("\nData and situation of block n. %d -->\n", i+1);
        int next = f->current_block->next_block;
        DiskDriver_readBlock(f->sfs->disk, tmp, next);
        FileBlock* p_tmp = (FileBlock*)tmp;
        memcpy(bufelse, p_tmp->data, BLOCK_SIZE  - sizeof(BlockHeader));
        printf(" PREVIOUS_IDX: %d\n NEXT_IDX: %d\n BLOCK_IN_FILE: %d\n", p_tmp->header.previous_block, p_tmp->header.next_block, p_tmp->header.block_in_file);
        printf("%s\n", bufelse);
        f->current_block = &(p_tmp->header);
    }
    f->pos_in_file = saved_prev_pos;
    f->current_block = &(f->fcb->header);
}

void aux(DirectoryHandle* d, int level){
    int res;

    for(int i=0; i<level; i++){
        if(i == level-1) printf("|----- ");
        else{
            printf("|      ");
        }
    }
    if(strcmp("/", d->dcb->fcb.name) == 0) printf("%s\n", d->dcb->fcb.name);
    else printf("%s/\n", d->dcb->fcb.name);

    level++;

    int num_entries = d->dcb->num_entries;
    if(num_entries == 0) return;
    char** names = (char**)malloc(sizeof(char*)*num_entries);
    for(int i=0; i<num_entries; i++) names[i] = (char*)malloc(128);  

    SimpleFS_readDir(names, d);

    for(int i=0; i<num_entries; i++){
        res = SimpleFS_changeDir(d, names[i]);
        if(res < 0) {
            for(int j=0; j<level; j++){
                if(j == level-1) printf("|----- ");
                else{
                    printf("|      ");
                }
            } 
            printf("%s\n", names[i]);
        }
        else{
            aux(d, level);
            res = SimpleFS_changeDir(d, "..");
        }
    } 

    for(int i=0; i<num_entries; i++) free(names[i]);
    free(names);
}

void SimpleFS_tree(SimpleFS* fs){
    int res;
    char buf[BLOCK_SIZE] = {0};
    res = DiskDriver_readBlock(fs->disk, buf, fs->root);
    if(res < 0) return;

    DirectoryHandle* tmp = SimpleFS_createDirHandle((FirstDirectoryBlock*)buf, fs);

    printf("Directories Tree -->\n\n");
    
    aux(tmp, 0);

    free(tmp->p_directory);
    free(tmp->dcb);
    free(tmp);
}

void BeEvilWhenTestingSimpleFS(SimpleFS* fs,DiskDriver* disk){

    printf("Situazione iniziale (no formattazione):\n\n");
    printDisk(disk);
    printf("\n");

    printf("\nSituazione dopo formattazione:\n");
    DirectoryHandle* curr_dir = SimpleFS_init(fs,disk);
    
    printDisk(disk);
    printf("\n");
    printDir(curr_dir);
    printf("\n");
    SimpleFS_tree(fs);
    printf("\n");
    
    char buf[40] = {0};
    printf("\n\nCreating the MainFolder\n");
    SimpleFS_mkDir(curr_dir, "MainFolder");
    for(int i = 0;i < 10; i++){
        sprintf(buf,"%d",i);
        SimpleFS_mkDir(curr_dir,buf);
    }
    SimpleFS_changeDir(curr_dir,"2");
    for(int i = 0;i < 10; i++){
        sprintf(buf,"%d",i+10);
        SimpleFS_mkDir(curr_dir,buf);
    }
    printDir(curr_dir);
    
    /*printf("\n\nEntering in the MainFolder\n");
    SimpleFS_changeDir(curr_dir, "MainFolder");*/
    SimpleFS_changeDir(curr_dir, "..");
    printDir(curr_dir);
    SimpleFS_tree(fs);
    /*printf("\n");

    FileHandle* file;
    printf("\nCreate file Prova.txt in currDir\n\n");
    file = SimpleFS_createFile(curr_dir, (char*)"Prova.txt");
    SimpleFS_syncCurrDir(curr_dir);
    SimpleFS_tree(fs);
    printf("\n");
	
    int size = 8187*BLOCK_SIZE;
    char* buffer = (char*)malloc(size);
    memset(buffer,'a',size);
    printf("\nWriting (8187*BLOCK_SIZE char) on Prova.txt\n\n");
    int res = SimpleFS_write(file, buffer, size);
    printf("Byte scritti: %d\n\n", res);
    SimpleFS_syncCurrDir(curr_dir);
    //printDisk(disk);
    //printf("\n");
    printDir(curr_dir);
    printf("\n");
    SimpleFS_tree(fs);
    printf("\n");
    free(buffer);

    printf("\n\nReturning in /...\n");
    SimpleFS_changeDir(curr_dir, "..");
    //printDisk(disk);
    //printf("\n");
    printDir(curr_dir);
    printf("\n");
    SimpleFS_tree(fs);
    printf("\n");

    printf("\n\nEntering in the MainFolder\n");
    SimpleFS_changeDir(curr_dir, "MainFolder");
    printDir(curr_dir);

    printf("\nClosing file Prova.txt...\n\n");
    SimpleFS_close(file);
    printf("\nOpening file Prova.txt...\n\n");
    file = SimpleFS_openFile(curr_dir, "Prova.txt");
    printFile(file);
    printf("\n");
    SimpleFS_close(file);

    printf("\nDeleting file Prova.txt...\n\n");
    SimpleFS_remove(curr_dir, "Prova.txt");
    //printDisk(disk);
    //printf("\n");
    printDir(curr_dir);
    printf("\n");
    SimpleFS_tree(fs);
    printf("\n");

    printf("\n\nReturning in /...\n");
    SimpleFS_changeDir(curr_dir, "..");
    printDir(curr_dir);
    printf("\n");
    SimpleFS_tree(fs);
    printf("\n");*/

    free(curr_dir->p_directory);
    free(curr_dir->dcb);
    free(curr_dir);

    DiskDriver_flush(disk);
}
