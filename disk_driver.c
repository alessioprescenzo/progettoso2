#include "disk_driver.h"

void DiskDriver_init(DiskDriver* disk, const char* filename, int num_blocks){
    
    int bitmap_entries = num_blocks%8?(num_blocks/8)+1:(num_blocks/8);  // how many bytes are needed to store the bitmap
    int aux = bitmap_entries - (BLOCK_SIZE - sizeof(DiskHeader));
    int bitmap_blocks = aux>0?(aux)/BLOCK_SIZE+2:1;   // how many blocks in the bitmap
    int filesize = num_blocks*BLOCK_SIZE;


    int fd = open(filename, O_CREAT | O_RDWR | O_EXCL , 0777);
    if(fd < 0){
        if(errno == EEXIST){
            fd = open(filename, O_RDWR, 0777);
            char* result = mmap(NULL, (bitmap_blocks)*BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
            disk->bitmap_data = result + sizeof(DiskHeader);
            disk->header = (DiskHeader*)result;
            disk->fd=fd;
            printf("Disk already created --> Just opening it\n\n");
            return;
        }
        else{
            perror("Error when opening the file\n");
            exit(-1);
        }
    }
    else{
        DiskHeader diskHeader_temp;
        diskHeader_temp.num_blocks = num_blocks;
        diskHeader_temp.bitmap_blocks = bitmap_blocks;
        diskHeader_temp.bitmap_entries = bitmap_entries;
        diskHeader_temp.free_blocks = num_blocks - bitmap_blocks;
        diskHeader_temp.first_free_block = bitmap_blocks;

        int res = lseek(fd, filesize-1, SEEK_SET);
        if(res != filesize-1){
            perror("Error in 'lseek2' function\n");
            exit(-1);
        }

        res = write(fd, "" , 1);
        if(!res){
            perror("Error in 'Write2' function\n");
            exit(-1);
        }

        res = lseek(fd, 0, SEEK_SET);
        if(res != 0){
            perror("Error in 'lseek3' function\n");
            exit(-1);
        }
        
        res = write(fd, &diskHeader_temp, sizeof(DiskHeader));
        if(!res){
            perror("Error in 'write header' function\n");
            exit(-1);
        }

        res = lseek(fd, sizeof(DiskHeader), SEEK_SET);
        if(res != sizeof(DiskHeader)){
            perror("Error in 'lseek 1' function\n");
            exit(-1);
        }

        char map[bitmap_entries];
        memset(map, 0 , bitmap_entries);
        int i;
        BitMap temp = {0};
        temp.entries=map;
        temp.num_bits=num_blocks;
        for(i=0;i<bitmap_blocks;i++){
            BitMap_set(&temp,i,1);
        }

        res = write(fd, map, bitmap_entries);
        if(!res){
            perror("Error in 'Write1' function\n");
            exit(-1);
        }

        res = lseek(fd, 0, SEEK_SET);
        if(res != 0){
            perror("Error in 'lseek4' function\n");
            exit(-1);
        }
        disk->fd = fd;
        char* result = mmap(NULL, (bitmap_blocks)*BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        disk->bitmap_data = result + sizeof(DiskHeader);
        disk->header = (DiskHeader*)result;
    }
}

int DiskDriver_readBlock(DiskDriver* disk, void* dest, int block_num){
    if(block_num >= disk->header->num_blocks || block_num < 0) {
        return -1;
    }
    BitMapEntryKey res = BitMap_blockToIndex(block_num);
    unsigned char bit = BitMap_isBusy(&res, disk->bitmap_data);
    if(bit == 0) {
        return -1;
    }

    if(pread(disk->fd, dest, BLOCK_SIZE, block_num*BLOCK_SIZE) < 0){
        return -1;
    } 

    return 0;
}

int DiskDriver_writeBlock(DiskDriver* disk, void* src, int block_num){
    if(block_num >= disk->header->num_blocks || block_num < 0) {
        return -1;
    }

    BitMapEntryKey res = BitMap_blockToIndex(block_num);
    unsigned char bit = BitMap_isBusy(&res, disk->bitmap_data);
    if(bit == 1) {
        return -1;
    }

    BitMap temp = {0};
    temp.entries = disk->bitmap_data;
    temp.num_bits = disk->header->num_blocks;

    //modified struct 
    BitMap_set(&temp, block_num, 1);
    disk->header->free_blocks -= 1;
    if(block_num == disk->header->first_free_block) disk->header->first_free_block = BitMap_get(&temp, block_num, 0);

    if(pwrite(disk->fd, src, BLOCK_SIZE, block_num*BLOCK_SIZE) < 0){
        return -1;
    } 

    return 0;
}

int DiskDriver_freeBlock(DiskDriver* disk, int block_num){
    if(block_num >= disk->header->num_blocks || block_num < 0) {
        return -1;
    }
    if(block_num < disk->header->bitmap_blocks){
        return -1;
    }
    
    BitMapEntryKey res = BitMap_blockToIndex(block_num);
    unsigned char bit = BitMap_isBusy(&res, disk->bitmap_data);
    if(bit == 0) return 0;
    else{
        BitMap temp = {0};
        temp.entries = disk->bitmap_data;
        temp.num_bits = disk->header->num_blocks;
        //wipe data inside that block needed free
        BitMap_set(&temp, block_num, 0);
        disk->header->free_blocks += 1;
        //save modified struct
        if(block_num < disk->header->first_free_block) disk->header->first_free_block = block_num;
        if(disk->header->first_free_block < 0) disk->header->first_free_block = block_num;
        
        unsigned char src[BLOCK_SIZE] = {0};
        memset(src, 0, BLOCK_SIZE);
        
        if(pwrite(disk->fd, src, BLOCK_SIZE, block_num*BLOCK_SIZE) < 0){
            return -1;
        }
        return 0; 
    }
}

int DiskDriver_updateBlock(DiskDriver* disk, void* src, int block_num){
    if(block_num >= disk->header->num_blocks || block_num < 0) {
        return -1;
    }

    int res;

    res = DiskDriver_freeBlock(disk, block_num);
    if(res < 0){
        printf("Error in freeBlock: updateBlock\n\n");
        exit(-1);
    }
    res = DiskDriver_writeBlock(disk, src, block_num);
    if(res < 0){
        printf("Error in writeBlock: updateBlock\n\n");
        exit(-1);
    }

    return 0;
}

int DiskDriver_getFreeBlock(DiskDriver* disk, int start){
    BitMap temp = {0};
    temp.entries = disk->bitmap_data;
    temp.num_bits = disk->header->num_blocks;

    int res = BitMap_get(&temp,start,0);
    if(res < 0){
        printf("Error in getFreeBlock\n\n");
        return -1;
    }
    return res;
}

int DiskDriver_flush(DiskDriver* disk){
    int res = msync((char*)disk->header, (disk->header->bitmap_blocks)*BLOCK_SIZE , MS_SYNC);
    if(res < 0){
        printf("Error flushing mmap'd data\n\n");
        return -1;
    }
    return res;
}

void BeEvilWhenTestingDiskDriver(DiskDriver* disk, const char* filename, int num_blocks){

    int res;

    DiskDriver_init(disk, filename, num_blocks);

    printf("Testing on Disk Driver...\n\n");
    
    DiskHeader* tmp = disk->header;
    printf("Situation:\n num_blocks = %d\n bitmap_blocks = %d\n bitmap_entries = %d\n free_blocks = %d\n first_free_blocks = %d\n FD: %d\n\n", 
    tmp->num_blocks, tmp->bitmap_blocks, tmp->bitmap_entries, tmp->free_blocks, tmp->first_free_block, disk->fd);
    
    BitMap bitMap_temp = {0};
    bitMap_temp.num_bits = disk->header->num_blocks;
    bitMap_temp.entries = disk->bitmap_data;
    
    BeEvilWhenTestingBitMap(&bitMap_temp);
    
    unsigned char dest[BLOCK_SIZE+1] = {0};
    unsigned char src[BLOCK_SIZE] = {0};
    memset(src, 65, BLOCK_SIZE);

    printf("\n\nTesting on Write/Read Block...\n\n");

    res = DiskDriver_readBlock(disk, dest, -4);
    if(res < 0) printf("Errore ReadBlock: idx_block = -4\n\n");
    else printf("Lettura riuscita: idx_block = -4\n dest = %s\n\n", dest);
    
    res = DiskDriver_readBlock(disk, dest, 20000);
    if(res < 0) printf("Errore ReadBlock: idx_block = 20000\n\n");
    else printf("Lettura riuscita: idx_block = 20000\n dest = %s\n\n", dest);

    res = DiskDriver_writeBlock(disk, src, -4);
    if(res < 0) printf("Errore WriteBlock: idx_block = -4\n\n");
    else {
        printf("Scrittura riuscita: idx_block = -4\n");
        res = DiskDriver_readBlock(disk, dest, -4);
        printf("%s\n\n", dest);
    }

    res = DiskDriver_writeBlock(disk, src, 20000);
    if(res < 0) printf("Errore WriteBlock: idx_block = 20000\n\n");
    else {
        printf("Scrittura riuscita: idx_block = 20000\n");
        res = DiskDriver_readBlock(disk, dest, 20000);
        printf("%s\n\n", dest);
    }

    res = DiskDriver_readBlock(disk, dest, 0);
    if(res < 0) printf("Errore ReadBlock: idx_block = 0\n\n");
    else {
        DiskHeader* read_diskHeader = (DiskHeader*)dest;
        printf("Situation DiskDriver_read:\n num_blocks = %d\n bitmap_blocks = %d\n bitmap_entries = %d\n free_blocks = %d\n first_free_blocks = %d\n\n", 
        read_diskHeader->num_blocks, read_diskHeader->bitmap_blocks, read_diskHeader->bitmap_entries, read_diskHeader->free_blocks, read_diskHeader->first_free_block);

    }

    res = DiskDriver_readBlock(disk, dest, 1);
    if(res < 0) printf("Errore ReadBlock: idx_block = 1\n\n");
    else printf("Lettura riuscita: idx_block = 1\n dest = %s\n\n", dest);
    
    res = DiskDriver_writeBlock(disk, src, 1);
    if(res < 0) printf("Errore WriteBlock: idx_block = 1\n\n");
    else {
        printf("Scrittura riuscita: idx_block = 1\n");
        res = DiskDriver_readBlock(disk, dest, 1);
        printf("%s\n\n", dest);
    }
    
    res = DiskDriver_writeBlock(disk, src, 6);
    if(res < 0) printf("Errore WriteBlock: idx_block = 6\n\n");
    else {
        printf("Scrittura riuscita: idx_block = 6\n");
        res = DiskDriver_readBlock(disk, dest, 6);
        printf("stringa: %s\n\n", dest);
    }

    printf("Situation DiskDriver_read:\n num_blocks = %d\n bitmap_blocks = %d\n bitmap_entries = %d\n free_blocks = %d\n first_free_blocks = %d\n\n", 
    tmp->num_blocks, tmp->bitmap_blocks, tmp->bitmap_entries, tmp->free_blocks, tmp->first_free_block);

    res = DiskDriver_writeBlock(disk, src, 3);
    if(res < 0) printf("Errore WriteBlock: idx_block = 3\n\n");
    else {
        printf("Scrittura riuscita: idx_block = 3\n");
        res = DiskDriver_readBlock(disk, dest, 3);
        printf("stringa: %s\n\n", dest);
    }

    printf("Situation DiskDriver_read:\n num_blocks = %d\n bitmap_blocks = %d\n bitmap_entries = %d\n free_blocks = %d\n first_free_blocks = %d\n\n", 
    tmp->num_blocks, tmp->bitmap_blocks, tmp->bitmap_entries, tmp->free_blocks, tmp->first_free_block);

    res = DiskDriver_writeBlock(disk, src, 2);
    if(res < 0) printf("Errore WriteBlock: idx_block = 2\n\n");
    else {
        printf("Scrittura riuscita: idx_block = 2\n");
        res = DiskDriver_readBlock(disk, dest, 2);
        printf("stringa: %s\n\n", dest);
    }

    printf("Situation DiskDriver_read:\n num_blocks = %d\n bitmap_blocks = %d\n bitmap_entries = %d\n free_blocks = %d\n first_free_blocks = %d\n\n", 
    tmp->num_blocks, tmp->bitmap_blocks, tmp->bitmap_entries, tmp->free_blocks, tmp->first_free_block);
    
    res = DiskDriver_freeBlock(disk,2); 
    if(res < 0) printf("Errore DiskDriver_Freeblock: idx_block = 2\n\n");
    else{
        printf("freeBlock Successful: idx_block = 2\n\n");        
        res = pread(disk->fd, dest, BLOCK_SIZE, 2*BLOCK_SIZE);
        printf("String after free: %s\n\n", dest);
    }

    printf("Situation DiskDriver_read:\n num_blocks = %d\n bitmap_blocks = %d\n bitmap_entries = %d\n free_blocks = %d\n first_free_blocks = %d\n\n", 
    tmp->num_blocks, tmp->bitmap_blocks, tmp->bitmap_entries, tmp->free_blocks, tmp->first_free_block);

    res = DiskDriver_getFreeBlock(disk,5);
    if(res < 0) printf("Error in DiskDriver_getFreeBlock: start = 5\n\n");
    else{
        printf("getFreeBlock successful: start = 5\n");
        printf("free block idx=%d\n\n",res);
    }

    res = DiskDriver_freeBlock(disk,1); 
    if(res < 0) printf("Errore DiskDriver_Freeblock: idx_block = 1\n\n");
    else{
        printf("freeBlock Successful: idx_block = 1\n\n");        
        res = pread(disk->fd, dest, BLOCK_SIZE, 1*BLOCK_SIZE);
        printf("String after free: %s\n\n", dest);
        printf("\n\n");
    }

    res = DiskDriver_getFreeBlock(disk,15);
    if(res < 0) printf("Error in DiskDriver_getFreeBlock: start = 15\n\n");
    else{
        printf("getFreeBlock successful: start = 15\n");
        printf("free block idx=%d\n\n",res);
    }

    res = DiskDriver_flush(disk);
    if(res < 0) printf("Error in DiskDriver_flush\n\n");
    else{
        printf("flush successful\n");
    }
}

void printDisk(DiskDriver* disk){
    printf("Situation of disk -->\n");
    printf(" DISKHEADER: %p\n BITMAP: %p\n FD: %d\n",
            disk->header, disk->bitmap_data, disk->fd);
    printf(" NUM_BLOCKS: %d\n BITMAP_BLOCKS: %d\n BITMAP_ENTRIES: %d\n FREE_BLOCKS: %d\n FIRST_FREE_BLOCK: %d\n",
            disk->header->num_blocks, disk->header->bitmap_blocks, disk->header->bitmap_entries, disk->header->free_blocks, disk->header->first_free_block);
}